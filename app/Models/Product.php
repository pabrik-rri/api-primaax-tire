<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\OrderDetail;
use DB;

class Product extends Model
{
    public $table = 'products';

    protected $primaryKey = 'id';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['code', 'description', 'min_qty', 'name', 'price_market', 'price_warehouse', 'product_category_id', 'qty', 'status', 'unit_id'];

    public function ProductCategory()
    {
        return $this->belongsTo( ProductCategory::class );
    }

    public function getBestSeller()
    {
        $bestSeller = OrderDetail::limit(10)
                        ->groupBy('product_id')
                        ->orderBy('product_id','DESC')
                        ->pluck('product_id');
        
        $product    = Product::whereIn('id', $bestSeller)
                            ->orderBy('id')
                            ->get();

        return $product;
    }

    public function getProductNew()
    {   
        $product    = Product::where('is_new',1)->orderBy('id','DESC')->get();
        return $product;
    }

    public function getPriceRegion($city = null, $productCategory = null)
    {
        if($city != "" && $city != "Bandung" && $city != "Kota Bandung" && $city != "Kab Bandung" && $city != "bandung") {
            $products       = ProductRegion::select([
                                    'products.id as id',
                                    'products.code as code',
                                    'products.name as name',
                                    'products.description as description',
                                    'product_regions.price as price_market',
                                    'products.image as image',
                                ])
                                ->join('products','product_regions.product_id', '=' ,'products.id')
                                ->join('cities','product_regions.city_id', '=' ,'cities.id')
                                ->where('products.product_category_id', $productCategory)
                                ->where('cities.city', 'like', '%'. $city .'%')
                                ->get();

            if(count($products) == 0) {
                $products       = Product::where('product_category_id', $productCategory)->get();
            }
        }

        return $products;
    }

    public function getDataWithPaginate($limit = null, $offset = null, $productCategory = null)
    {
        $products = Product::offset($offset)
                    ->limit($limit);

        if($productCategory != null)
        {
            $products->where('product_category_id', $productCategory);
        }

        return $products->get();
    }

    public function getCountData($productCategory = null)
    {
        $count = $productCategory != null ? Product::where('product_category_id', $productCategory)->count() : Product::count();

        return $count;
    }

    public function getDataNew()
    {
        $new = Product::select([
                        'products.id as id',
                        'products.code as code',
                        'products.name as name',
                        'products.product_category_id as product_category_id',
                        'categories.name as category',
                        'products.image as image',
                    ])
                    ->join('units','products.unit_id','=' ,'units.id')
                    ->join('product_categories','products.product_category_id','=' ,'product_categories.id')
                    ->join('categories','product_categories.category_id','=' ,'categories.id')
                    ->orderBy('products.created_at','DESC')
                    ->limit(10)
                    ->get();
        
        return $new;
    }

    public function getCountCategory($productCategory = null)
    {
        $count = $productCategory != null ? Product::where('product_category_id', $productCategory)->count() : Product::count();

        return $count;
    }
}
