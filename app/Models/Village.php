<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    public $table = 'villages';

    public function District()
    {
        return $this->belongsTo(\App\Models\District::class, 'district_id', 'id');
    }
}
