<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    public $table = 'cms_news';

    public function User()
    {
        return $this->belongsTo('\App\User', 'user_created', 'id');
    }

    public function getDataWithPaginate($limit = null, $offset = null)
    {
        $news = News::orderBy('created_at','DESCT')
                    ->offset($offset)
                    ->limit($limit)
                    ->get();
        
        return $news;
    }

    public function getCountData()
    {
        $count = News::count();

        return $count;
    }
}
