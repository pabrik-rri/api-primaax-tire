<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Voucher extends Model
{
    public $table = 'vouchers';

    protected $fillable = ['code', 'description', 'start', 'finish', 'value'];
}
