<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Store extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'stores';

    protected $fillable = [
    	'name',
    	'no_id_card',
    	'gender',
    	'birth_date',
    	'email',
        'address',
        'longitude',
        'latitude',
    	'city_id',
    	'mobile',
    	'zip_code',
    	'name_store',
    	'password',
    	'status',
        'is_courier',
        'mitra_id',
        'avatar',
        'token_fcm'
    ];


    public function getCity()
    {
        return $this->belongsTo(\App\Models\City::class, 'city_id', 'id');
    }

    public function Mitra()
    {
        return $this->belongsTo('\App\Models\Mitra', 'mitra_id', 'id');
    }

    public function getAllData()
    {
        return Store::orderBy('name','ASC')->get();
    }

    public function getDataPartner($latitude = null, $longitude = null, $city = null, $store = null)
    {
        //get nearby location
        if($latitude != null && $longitude != null)
        {
            $partners = Store::select([
                        'id',
                        'name',
                        'latitude',
                        'longitude',
                        \DB::raw('( 3959 * acos( cos( radians(' . $latitude . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $longitude . ') ) + sin( radians(' . $latitude .') ) * sin( radians(latitude) ) ) ) AS distance')
                    ])
                    ->where('is_courier', 0)
                    // ->having('distance','<', 1.864)
                    ->orderBy('distance')
                    ->get();
        }

        //get location with city
        if($city != null)
        {
            $partners = Store::join('cities', 'stores.city_id', '=', 'cities.id')
                        ->where('city','like','%'. $city .'%')
                        ->where('is_courier', 0)
                        ->orderBy('name','ASC')
                        ->get();            
        }

        //get location with store
        if($store != null)
        {
            $partners = Store::where('name','like','%'. $store .'%')  
                        ->where('is_courier', 0)
                        ->orderBy('name','ASC')->get();            
        }

        //get all location partner
        if($store == null && $city == null && $latitude == null && $longitude == null) {
            $partners = Store::where('is_courier', 0)
                        ->orderBy('name','ASC')->get();     
        }

        return $partners;
    }

    public function getDataBank($storeId = null)
    {
        return Store::select([
                        'bank_logo',
                        'bank_name',
                        'bank_norek',
                        'bank_account'
                    ])
                    ->where('id', $storeId)->first();
    }
}
