<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Mitra extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'mitras';

    protected $fillable = [
    	'name',
    	'no_id_card',
    	'gender',
    	'birth_date',
    	'email',
        'address',
        'longitude',
        'latitude',
    	'city_id',
    	'mobile',
    	'zip_code',
    	'name_store',
    	'password',
    	'status',
        'is_courier',
        'mitra_id',
        'avatar',
        'token_fcm'
    ];


    public function getCity()
    {
        return $this->belongsTo(\App\Models\City::class, 'city_id', 'id');
    }

    public function Mitra()
    {
        return $this->belongsTo('\App\Models\Mitra', 'mitra_id', 'id');
    }
}
