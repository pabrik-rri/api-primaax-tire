<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DebtDetails extends Model
{
    protected $table = 'debt_details';

    public function Receivables()
    {
        return $this->belongsTo('\App\Models\Receivables', 'receivable_id', 'id');
    }
}