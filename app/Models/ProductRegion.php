<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class ProductRegion extends Model
{
    public $table = 'product_regions';

    public function Product()
    {
        return $this->belongsTo(\App\Models\Product::class, 'product_id', 'id');
    }
}
