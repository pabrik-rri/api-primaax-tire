<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderHistory extends Model
{
    protected $table = 'order_histories';

    protected $fillable = [
    	'order_id',
    	'order_detail_id',
    	'qty_old',
    	'qty',
    	'user_id',
	];
}
