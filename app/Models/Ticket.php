<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    public $table = 'tickets';

    public function Customer()
    {
        return $this->belongsTo(\App\Models\Customer::class, 'customer_id', 'id');
    }

    public function Agent()
    {
        return $this->belongsTo(\App\Models\Mitra::class, 'mitra_id', 'id');
    }

    public function Store()
    {
        return $this->belongsTo(\App\Models\Store::class, 'store_id', 'id');
    }
}
