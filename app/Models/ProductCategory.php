<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class ProductCategory extends Model
{
    public $table = 'product_categories';

    protected $dates = ['created_at', 'updated_at'];

    protected $fillable = ['name', 'status', 'category_id','images'];

    public function Category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'category_id', 'id');
    }
}
