<?php

namespace App\Models;

use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;

class Customer extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    protected $table = 'customers';

    protected $fillable = [
    	'name',
    	'no_id_card',
    	'gender',
    	'birth_date',
    	'email',
        'address',
        'longitude',
        'latitude',
    	'city_id',
    	'mobile',
    	'zip_code',
    	'name_store',
    	'password',
    	'status',
        'avatar',
        'token_fcm',
        'referral_code'
    ];

    public function scopeCourier( $query ){
        $data = $query->where('is_courier', 1);
        return $data;
    }

    public function getCity()
    {
        return $this->belongsTo(\App\Models\City::class, 'city_id', 'id');
    }
}
