<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receivables extends Model
{
    protected $table = 'receivables';

    protected $fillable = [
    	'supplier_name',
    	'customer_name',
    	'invoice_date',
    	'code',
    	'is_factory',
    	'value_of_receivable',
    	'balance_of_receivable'
	];
	
    public function Order()
    {
        return $this->belongsTo(\App\Models\Order::class, 'order_id', 'id');
    }
}