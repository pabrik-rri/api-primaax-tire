<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class ProductMitra extends Model
{
    public $table = 'product_mitras';

    public function Product()
    {
        return $this->belongsTo(\App\Models\Product::class, 'product_id', 'id');
    }

    public function getDataStock($mitraId)
    {
        $stock = ProductMitra::select([
                        'products.id as id',
                        'products.name as name',
                        'products.product_category_id as product_category_id',
                        'product_mitras.qty as qty',
                        'product_mitras.min_qty as min_qty',
                        'units.name as unit',
                    ])
                    ->join('products','product_mitras.product_id','=' ,'products.id')
                    ->join('units','products.unit_id','=' ,'units.id')
                    ->orderBy('product_mitras.created_at','DESC')
                    ->where('mitra_id', $mitraId)
                    ->get();
        
        return $stock;
    }
}
