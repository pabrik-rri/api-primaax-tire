<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ManagementStore extends Model
{
    public $table = 'management_stores';
}
