<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public $table = 'districts';

    public function City()
    {
        return $this->belongsTo(\App\Models\City::class, 'city_id', 'id');
    }
}
