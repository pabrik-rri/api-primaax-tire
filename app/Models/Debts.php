<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Debts extends Model
{
    protected $table = 'debts';

    public function DebtDetails()
    {
        return $this->hasMany('\App\Models\DebtDetails', 'debt_id', 'id');
    }
}