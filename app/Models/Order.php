<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Session;

use DB;
use App\Models\Receivables;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
    	'order_code',
        'user_id',
    	'mitra_id',
    	'payment',
    	'status_payment',
    	'status',
    	'address',
        'address_note',
        'longitude',
        'latitude',
        'ongkir',
        'voucher_id',
        'courier_id',
        'discount',
        'review',
        'star',
        'delivery_date'
    ];


    public static function getAutoNumber( $type , $is_po)
    {
        $result = self::orderBy('id', 'desc');

        if($is_po == 1) { 
            $result->where('is_po', 1); 
        } else {
            $result->where('is_po', 0); 
        }

        $result = $result->first();

        if ($result) {
            $lastNumber = (int) substr($result->order_code, 0, 5);
            $newNumber = $lastNumber + 1;

            if (strlen($newNumber) == 1) {
                $newNumber = '0000'.$newNumber;
            } elseif (strlen($newNumber) == 2) {
                $newNumber = '000'.$newNumber;
            } elseif (strlen($newNumber) == 3) {
                $newNumber = '00'.$newNumber;
            } else {
                $newNumber = $newNumber;
            }

            $currMonth = (int)date('m', strtotime($result->order_code));
            $currYear = (int)date('y', strtotime($result->order_code));
            $nowMonth = (int)date('m');
            $nowYear = (int)date('y');

            if ( ($currMonth < $nowMonth && $currYear == $nowYear) || ($currMonth == $nowMonth && $currYear < $nowYear) ) {
                $newNumber = '00001';
            } else {
                $newNumber = $newNumber;
            }

            $newCode = $newNumber."/".$type."/".date('m')."/".date('Y');
        } else {
            $newCode = "00001/".$type."/".date('m')."/".date('Y');
        }

        return $newCode;
    }

    public function getNumberBalance()
    {
        $last_receivables = Receivables::where('code', 'like', '%/'.date('n').'/'.date('Y'))->orderBy('code', 'desc')->first();
        if (!$last_receivables) {
            $code = '00001/FB/' . date('n') . '/' . date('Y');
        } else {
            $ex = explode('/', $last_receivables->code);
            $increment = $ex[0];
            if (date('n') == $ex[2] && date('Y') == $ex[3])
                $code = str_pad($increment + 1, 5, '0', STR_PAD_LEFT) . '/FB/' . date('n') . '/' . date('Y');
            else
                $code = '00001/FB/' . date('n') . '/' . date('Y');
        }

        return $code;
    }

    public static function getByDistance($lat, $lng, $distance, $except = array())
    {
      $results = \DB::table('stores')
                    ->select([
                        'id',
                        \DB::raw('( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( latitude ) ) * cos( radians( longitude ) - radians(' . $lng . ') ) + sin( radians(' . $lat .') ) * sin( radians(latitude) ) ) ) AS distance')
                    ])
                    ->where('is_courier', 0)
                    ->where('status', 1)
                    ->where('status_store', 1)
                    ->having('distance','<',$distance)
                    ->orderBy('distance')->limit(1);


      if(sizeof($except) > 0)
      {
          $results = $results->whereNotIn('id', $except);

          app('session')->push('mitra_id', $except);
      }

      $data = $results->get();

      if(sizeof($data) > 0)
      {
        return $data[0]->id;
      }

      return false;
    }

    public function relations()
    {
        $mitra      = new Mitra;
        $customer   = new Customer;
        $courier    = new Mitra;

        return $this
                ->select(
                    $this->table.'.id',
                    $this->table.'.order_code',
                    $this->table.'.created_at as order_date',
                    \DB::raw('
                      CASE WHEN orders.status = \'0\' THEN "Pending"
                      WHEN orders.status = \'1\' THEN "Diterima"
                      WHEN orders.status = \'2\' THEN "Persiapan Pengiriman"
                      WHEN orders.status = \'3\' THEN "Dalam Pengiriman"
                      WHEN orders.status = \'4\' THEN "Batal"
                      WHEN orders.status = \'5\' THEN "Selesai" ELSE 0 END as order_status'),
                    $this->table.'.address as order_address',
                    $this->table.'.address_note as address_note',
                    $this->table.'.latitude as latitude',
                    $this->table.'.longitude as longitude',
                    $this->table.'.star as stars',
                    $this->table.'.review as reviews',
                    $this->table.'.note as note',
                    $this->table.'.discount as discount',
                    $this->table.'.ongkir as ongkir',
                    \DB::raw('SUM(products.price_market * order_details.qty) - orders.discount as total_price'),
                    'mitra.name as mitra',
                    'customer.name as customer',
                    'customer.referral_code as customer_referral',
                    'courier.name as courier'
                )
                ->join('mitras as mitra', 'orders.mitra_id','=','mitra.id')
                ->join('customers as customer', 'orders.user_id','=','customer.id')
                ->leftJoin('mitras as courier', 'orders.courier_id','=','courier.id')->join('order_details','orders.id', '=' ,'order_details.order_id')
                ->join('products','order_details.product_id', '=' ,'products.id')
                ->groupBy('id');
    }

    public function getDataHome($id, $role = '', $is_courier)
    {
        $cond   = "mitra_id";

        if($role == "customer") { $cond = "user_id"; }

        $results = Order::select([
                        '*',
                        'orders.id as order_id',
                        'orders.created_at as order_date',
                        'orders.updated_at as finish_date',
                        DB::raw('SUM(order_details.price * order_details.qty) as total_price')
                    ])
                    ->where($cond, $id)
                    ->join('order_details','orders.id', '=' ,'order_details.order_id')
                    ->join('products','order_details.product_id', '=' ,'products.id')
                    ->orderBy('orders.created_at', 'DESC')
                    ->groupBy('orders.id');

        if($role == "mitra"){
            $results = $results->orWhere('orders.upline_id', $id);
        }

        if($role == "customer") {
            $results = $results->where('orders.status', 5);
        } else {
            
            if($is_courier == 1) {
                $results = $results->where('orders.is_po', 0)->whereNotIn('orders.status', [4,5]);
            } else {
                $results = $results->whereNotIn('orders.status', [4,5]);
            }
            
        }

        return $results->orderBy('orders.created_at','DESC')->limit(3)->get();
    }

    public function getIncomes($id, $start, $finish)
    {
        $results = Order::select([
                        '*',
                        'orders.id as order_id',
                        'orders.created_at as order_date',
                        'orders.updated_at as finish_date',
                        DB::raw('SUM(order_details.price * order_details.qty) as subtotal')
                    ])
                    ->where('status',5)
                    ->where('orders.is_po', 0)
                    ->where('mitra_id', $id)
                    ->whereBetween(DB::raw('DATE(orders.created_at)'), [$start, $finish])
                    ->join('order_details','orders.id', '=' ,'order_details.order_id')
                    ->join('products','order_details.product_id', '=' ,'products.id')
                    ->orderBy('orders.created_at', 'DESC')
                    ->groupBy('orders.id');

        $total = Order::select([
                        DB::raw('SUM(order_details.price * order_details.qty) as total')
                    ])
                    ->where('status',5)
                    ->where('orders.is_po', 0)
                    ->where('mitra_id', $id)
                    ->whereBetween(DB::raw('DATE(orders.created_at)'), [$start, $finish])
                    ->join('order_details','orders.id', '=' ,'order_details.order_id')
                    ->join('products','order_details.product_id', '=' ,'products.id')
                    ->orderBy('orders.created_at', 'DESC');

        $data['data'] = $results->get();
        $data['total'] = $total->first();

        return $data;
    }

    public function getDataOrder($id, $role = '', $is_courier)
    {
        $cond   = "mitra_id";

        if($role == "customer") { $cond = "user_id"; }

        $results = Order::select([
                        '*',
                        'orders.id as order_id',
                        'orders.created_at as order_date',
                        'orders.updated_at as finish_date',
                        DB::raw('SUM(order_details.price * order_details.qty) as total_price')
                    ])
                    ->whereNotIn('orders.status', [4,5])
                    ->where($cond, $id)
                    ->join('order_details','orders.id', '=' ,'order_details.order_id')
                    ->join('products','order_details.product_id', '=' ,'products.id')
                    ->orderBy('orders.created_at', 'DESC')
                    ->groupBy('orders.id');

        if($role == "mitra"){
            $results = $results->orWhere('orders.upline_id', $id);
        }

        if($is_courier == 1) {
            $results = $results->where('orders.is_po', 0)->whereNotIn('orders.status', [4,5]);
        }

        return $results->get();
    }

    public function getDataHistory($id, $role = '', $is_courier)
    {
        $cond   = "mitra_id";

        if($role == "customer") { $cond = "user_id"; }

        $results = Order::select([
                        '*',
                        'orders.id as order_id',
                        'orders.created_at as order_date',
                        'orders.updated_at as finish_date',
                        DB::raw('SUM(order_details.price * order_details.qty) as total_price')
                    ])
                    ->whereIn('orders.status', [4,5])
                    ->where($cond, $id)
                    ->join('order_details','orders.id', '=' ,'order_details.order_id')
                    ->join('products','order_details.product_id', '=' ,'products.id')
                    ->orderBy('orders.created_at', 'DESC')
                    ->groupBy('orders.id');

        if($role == "mitra"){
            $results = $results->orWhere('orders.upline_id', $id);
        }

        if($is_courier == 1) {
            $results = $results->where('orders.is_po', 0);
        }

        return $results->get();
    }

    public function getDataDetail($id)
    {
        $results = Order::select([
                        '*',
                        'orders.id as order_id',
                        'orders.status as status_order',
                        'orders.created_at as order_date',
                        'orders.updated_at as finish_date',
                        DB::raw('SUM(order_details.price * order_details.qty) as total_price')
                    ])
                    ->where('orders.id', $id)
                    ->join('order_details','orders.id', '=' ,'order_details.order_id')
                    ->orderBy('orders.created_at', 'DESC')
                    ->groupBy('orders.id');

        return $results->first();
    }

    public function getIncome($id)
    {
        return $this->select([
                        \DB::raw('SUM(order_details.price * order_details.qty) as income_today')
                      ])
                      ->join('order_details','orders.id', '=' ,'order_details.order_id')
                      ->where('is_po', 0)
                      ->where('orders.mitra_id', $id)
                      ->whereDate('orders.created_at','=', date('Y-m-d'))
                      ->orderBy('orders.created_at','DESC')
                      ->groupBy('orders.id')
                      ->first();
    }

    public function getSold($id)
    {
        return $this->select([
                        \DB::raw('SUM(order_details.price * order_details.qty) as income_today')
                      ])
                      ->join('order_details','orders.id', '=' ,'order_details.order_id')
                      ->where('is_po', 0)
                      ->where('orders.mitra_id', $id)
                      ->whereDate('orders.created_at','=', date('Y-m-d'))
                      ->orderBy('orders.created_at','DESC')
                      ->groupBy('orders.id')
                      ->first();
    }

    public function getLimitUse($id, $level)
    {
        $result = Order::select([
                            DB::raw('SUM(order_details.price * order_details.qty) as limit_use')
                        ])
                        ->join('order_details','orders.id', '=' ,'order_details.order_id')
                        ->where('is_po',1)
                        ->where('orders.mitra_id', $id);

        $result->first();
        
        $limit_use = isset($result->limit_use) ? $result->limit_use : 0;

        return $limit_use;
    }

    public function getBalanceUser($mitraId = null, $orderId = null)
    {
        $result = Order::select([
                            DB::raw('SUM(order_details.price * order_details.qty) as balance')
                        ])
                        ->join('order_details','orders.id', '=' ,'order_details.order_id')
                        ->where('is_po',1)
                        ->where('orders.id', $orderId)
                        ->where('orders.mitra_id', $mitraId)
                        ->first();
        
        $balance = isset($result->balance) ? $result->balance : 0;
        
        return $balance;
    }


    public function Product()
    {
        return $this->belongsTo('\App\Models\Product', 'product_id', 'id');
    }

    public function Customer()
    {
        return $this->belongsTo('\App\Models\Customer', 'user_id', 'id');
    }

    public function Mitra()
    {
        return $this->belongsTo('\App\Models\Mitra', 'mitra_id', 'id');
    }

    public function Courier()
    {
        return $this->belongsTo('\App\Models\Mitra', 'courier_id', 'id');
    }

}
