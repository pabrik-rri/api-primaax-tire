<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddressBook extends Model
{
    public $table = 'address_books';

    protected $fillable = [ 'address', 'longitude', 'latitude' ];
}
