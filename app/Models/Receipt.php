<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $table = 'receivable_pays';

    public function Receivables()
    {
        return $this->belongsTo(\App\Models\Receivables::class, 'receivable_id', 'id');
    }
}