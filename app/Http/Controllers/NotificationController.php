<?php

namespace App\Http\Controllers;

use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

use App\Models\Mitra;
use App\Models\Notification;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\Category;

class NotificationController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token       = $request->token;
        $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

        $this->user        = Mitra::find($this->credentials->sub);
        $this->cdn         = config('app.cdn');

        //FCM URL
        $this->fcmUrl   = "https://fcm.googleapis.com/fcm/send";
        $this->apiKey   = "AAAAnC_C3mI:APA91bHl5YVXJD0wRc_Ynotsj-y8AcYGUgX9AZxPtw5XnuzmNHxaIjnfyPrMuB_hwIE4h8zcFFc77mMSIUhVILpJfZFftBCAd511N0Kob-kYCjVhSnvo03KmJx3n2U2h1fp_CDTubjmW";
    }

    public function index()
    {
        try {
            $user  = Mitra::find($this->user->id);

            if ($user->role == "Courier") {
                $user_id = $user->mitra_id;
            } else {
                $user_id = $this->user->id;
            }

            $notif = Notification::where('user_id',$user_id)->orderBy('created_at','DESC')->get();

            $data  = array();

            if(sizeof($notif) > 0)
            {
                foreach ($notif as $key => $value) {

                    $icon   = $value->image == "" ? url('images/notif/default.png') : $value->image;

                    $data[$key]['id']            = $value->id;
                    $data[$key]['title']         = $value->title;
                    $data[$key]['description']   = $value->description;
                    $data[$key]['type']          = $value->type;
                    $data[$key]['icon']          = $icon;
                    $data[$key]['order_id']      = $value->order_id;
                    $data[$key]['created_at']    = date_format($value->created_at,'Y-m-d H:i:s');
                }

                return response()->json(['code' => 200, 'message' => 'success', 'data' => $data], 200);
            } else {
                return response()->json(['code' => 200, 'message' => 'data notifikasi tidak tersedia', 'data' => [] ], 200);
            }
        } catch (\Exception $e) {
            return response()->json(['code' => 500, 'message' => 'Gagal mengambil data notifikasi', 'data' => $data], 500);
        }
    }

    public function customer(Request $request)
    {
        try {
            $token       = $request->token;
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

            $notif = Notification::where('user_id',$credentials->sub)->orderBy('created_at','DESC')->get();

            $data  = array();

            if(sizeof($notif) > 0)
            {
                foreach ($notif as $key => $value) {

                    $icon   = $value->image == "" ? url('images/notif/default.png') : $value->image;

                    $data[$key]['id']            = $value->id;
                    $data[$key]['title']         = $value->title;
                    $data[$key]['description']   = $value->description;
                    $data[$key]['type']          = $value->type;
                    $data[$key]['icon']          = $icon;
                    $data[$key]['order_id']      = $value->order_id;
                    $data[$key]['created_at']    = date_format($value->created_at,'Y-m-d H:i:s');
                }
                return response()->json(['message' => 'success', 'data' => $data], 200);
            } else {
                return response()->json(['message' => 'Data notifikasi tidak tersedia', 'data' => [] ], 200);
            }
        } catch(\Exception $e) {
            return response()->json(['message' => 'Gagal mengambil data notifikasi', 'data' => $data], 404);
        }
    }

    public function sendNotif($token, $title, $description, $type, $id, $order_id)
    {
        $fcmUrl                 = $this->fcmUrl;
        $apiKey                 = $this->apiKey;

        $detail                 = OrderDetail::where('order_id',$order_id)->first();
        $category               = isset($detail->Product->ProductCategory->Category) ? $detail->Product->ProductCategory->Category->banner : "";

        $avatar                 = $category == "" ? $this->cdn.'/no_image_available.png' : $images = $this->cdn.'/categories/'. $category;


        $extraNotificationData  = [
            "title"         => $title,
            "message"       => "Order Baru telah masuk ke toko anda.",
            "order_id"      => $order_id
        ];

        $user                   = Mitra::where('is_courier',1)
                                    ->whereNotNull('token_fcm')
                                    ->where('mitra_id', $id)
                                    ->pluck('token_fcm')->toArray();

        array_push($user, $token);

        $tokenList              = (array) $user;

        $fcmNotification = [
            'registration_ids' => $tokenList, //multple token array
            // 'to'             => $token, //single token
            // 'notification'       => $notification,
            'priority'          => 'high',
            'delay_while_idle'  => false,
            'time_to_live'      => 0,
            'data'              => $extraNotificationData
        ];

        $fields = json_encode($fcmNotification);

        $headers = [
            'Authorization: key='. $apiKey,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);

        curl_close($ch);

        $notification                   = new Notification;
        $notification->title            = $title;
        $notification->description      = $description;
        $notification->type             = "Order";
        $notification->is_read          = 0;
        $notification->image            = $avatar;
        $notification->user_id          = $id;
        $notification->order_id         = $order_id;
        $notification->save();

        $courier    = Mitra::where('mitra_id', $id)->where('is_courier',1)->get();

        if(sizeof($courier) > 0)
        {
            foreach ($courier as $key => $value) {
                $notification                   = new Notification;
                $notification->title            = $title;
                $notification->description      = $description;
                $notification->type             = "Order";
                $notification->is_read          = 0;
                $notification->image            = $avatar;
                $notification->user_id          = $value->id;
                $notification->order_id         = $order_id;
                $notification->save();
            }
        }

        return true;
    }

    public function sendAccept($token, $title, $description,  $type, $id, $order_id, $mitra_id)
    {
        $fcmUrl                 = $this->fcmUrl;
        $apiKey                 = $this->apiKey;

        $order                  = Order::find($order_id);

        // $courier                = Mitra::find($this->user->id);
        $mitra                  = Mitra::find($mitra_id);

        $avatar                 = url('storage/mitra/avatar/'.$mitra->avatar);

        $extraNotificationData  = [
            "title"         => $title,
            "message"       => "Order Anda dengan kode : ". $order->order_code ." dalam persiapan.",
            "order_id"      => $order_id
        ];

        $fcmNotification = [
            // 'registration_ids' => $tokenList, //multple token array
            'to'                => $token, //single token
            // 'notification'       => $notification,
            'priority'          => 'high',
            'delay_while_idle'  => false,
            'time_to_live'      => 0,
            'data'              => $extraNotificationData
        ];

        $fields = json_encode($fcmNotification);

        $headers = [
            'Authorization: key='. $apiKey,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);

        curl_close($ch);


        $notification                   = new Notification;
        $notification->title            = $title;
        $notification->description      = $description;
        $notification->type             = "Order";
        $notification->is_read          = 0;
        $notification->image            = $avatar;
        $notification->user_id          = $id;
        $notification->order_id         = $order_id;
        $notification->save();

        return true;
    }

    public function sendReject($token, $title, $description,  $type, $id, $order_id, $mitra_id)
    {
        $fcmUrl                 = $this->fcmUrl;
        $apiKey                 = $this->apiKey;

        $order                  = Order::find($order_id);

        // $courier                = Mitra::find($this->user->id);
        $mitra                  = Mitra::find($mitra_id);

        $avatar                 = url('storage/mitra/avatar/'.$mitra->avatar);

        $extraNotificationData  = [
            "title"         => $title,
            "message"       => "Order Anda dengan kode : ". $order->order_code ." ditolak.",
            "order_id"      => $order_id
        ];

        $fcmNotification = [
            // 'registration_ids' => $tokenList, //multple token array
            'to'                => $token, //single token
            // 'notification'       => $notification,
            'priority'          => 'high',
            'delay_while_idle'  => false,
            'time_to_live'      => 0,
            'data'              => $extraNotificationData
        ];

        $fields = json_encode($fcmNotification);

        $headers = [
            'Authorization: key='. $apiKey,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);

        curl_close($ch);

        $notification                   = new Notification;
        $notification->title            = $title;
        $notification->description      = $description;
        $notification->type             = "Order";
        $notification->is_read          = 0;
        $notification->image            = $avatar;
        $notification->user_id          = $id;
        $notification->order_id         = $order_id;
        $notification->save();

        return true;
    }

    public function sendAdjustment($token, $title, $description,  $type, $id, $order_id, $mitra_id)
    {
        $fcmUrl                 = $this->fcmUrl;
        $apiKey                 = $this->apiKey;

        $order                  = Order::find($order_id);
        // $courier                = Mitra::find($this->user->id);
        $mitra                  = Mitra::find($mitra_id);

        $avatar                 = url('storage/mitra/avatar/'.$mitra->avatar);

        $extraNotificationData  = [
            "title"         => $title,
            "message"       => "Order Anda dengan kode : ". $order->order_code ." sedang dalam proses pengiriman oleh kurir ". $mitra->name,
            "order_id"      => $order_id
        ];

        $fcmNotification = [
            // 'registration_ids' => $tokenList, //multple token array
            'to'                => $token, //single token
            // 'notification'       => $notification,
            'priority'          => 'high',
            'delay_while_idle'  => false,
            'time_to_live'      => 0,
            'data'              => $extraNotificationData
        ];

        $fields = json_encode($fcmNotification);

        $headers = [
            'Authorization: key='. $apiKey,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);

        curl_close($ch);

        $notification                   = new Notification;
        $notification->title            = $title;
        $notification->description      = $description;
        $notification->type             = "Order";
        $notification->is_read          = 0;
        $notification->image            = $avatar;
        $notification->user_id          = $id;
        $notification->order_id         = $order_id;
        $notification->save();

        return true;
    }

    public function sendDelivery($token, $title, $description, $type, $id, $order_id, $mitra_id)
    {
        $fcmUrl                 = $this->fcmUrl;
        $apiKey                 = $this->apiKey;

        $order                  = Order::find($order_id);
        // $courier                = Mitra::find($this->user->id);
        $mitra                  = Mitra::find($mitra_id);

        $avatar                 = url('storage/mitra/avatar/'.$mitra->avatar);

        $extraNotificationData  = [
            "title"         => $title,
            "message"       => "Order Anda dengan kode : ". $order->order_code ." sudah diterima.",
            "order_id"      => $order_id
        ];

        $fcmNotification = [
            // 'registration_ids' => $tokenList, //multple token array
            'to'                => $token, //single token
            // 'notification'       => $notification,
            'priority'          => 'high',
            'delay_while_idle'  => false,
            'time_to_live'      => 0,
            'data'              => $extraNotificationData
        ];

        $fields = json_encode($fcmNotification);

        $headers = [
            'Authorization: key='. $apiKey,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);

        curl_close($ch);

        $notification                   = new Notification;
        $notification->title            = $title;
        $notification->description      = $description;
        $notification->type             = "Order";
        $notification->is_read          = 0;
        $notification->image            = $avatar;
        $notification->user_id          = $id;
        $notification->order_id         = $order_id;
        $notification->save();

        return true;
    }

    public function sendCancel($token, $title, $description, $type, $id, $order_id, $mitra_id)
    {
        $fcmUrl                 = $this->fcmUrl;
        $apiKey                 = $this->apiKey;

        $order                  = Order::find($order_id);
        $mitra                  = Mitra::find($mitra_id);

        $avatar                 = url('storage/mitra/avatar/'.$mitra->avatar);

        $extraNotificationData  = [
            "title"         => $title,
            "message"       => "Order Anda dengan kode : ". $order->order_code ." dibatalkan.",
            "order_id"      => $order_id
        ];

        $fcmNotification = [
            // 'registration_ids' => $tokenList, //multple token array
            'to'                => $token, //single token
            // 'notification'       => $notification,
            'priority'          => 'high',
            'delay_while_idle'  => false,
            'time_to_live'      => 0,
            'data'              => $extraNotificationData
        ];

        $fields = json_encode($fcmNotification);

        $headers = [
            'Authorization: key='. $apiKey,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
        $result = curl_exec($ch);

        curl_close($ch);

        $notification                   = new Notification;
        $notification->title            = $title;
        $notification->description      = $description;
        $notification->type             = "Order";
        $notification->is_read          = 0;
        $notification->image            = $avatar;
        $notification->user_id          = $id;
        $notification->order_id         = $order_id;
        $notification->save();

        return true;
    }
}
