<?php

namespace App\Http\Controllers\Cms;

use App\Models\Store;
use Laravel\Lumen\Routing\Controller as BaseController;

class StoreController extends BaseController
{
    public function __construct()
    {
        $this->modelStores = new Store;
    }

    public function index()
    {
        try {
            $cities        = $this->modelStores->getAllData();

            if($cities)
            {
                $data   = array();

                if(sizeof($cities) > 0)
                {
                    foreach ($cities as $key => $value) {
                        $data[$key]['id']          = $value->id;
                        $data[$key]['name']        = $value->name;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil data toko"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }
}
