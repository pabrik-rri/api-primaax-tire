<?php

namespace App\Http\Controllers\Cms;

use App\Models\Sosmed;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class SosmedController extends BaseController
{
    public function __construct(Request $request)
    {
        $this->cdnSosmed = config('app.cdn')."sosmed/";
    }

    public function index(Request $request)
    {
        try {
            $sosmed        = Sosmed::where('status',1)
                                ->orderBy('id','DESC')->get();

            if($sosmed)
            {
                $data   = array();

                if(sizeof($sosmed) > 0)
                {
                    foreach ($sosmed as $key => $value) {
                        $data[$key]['id']           = $value->id;
                        $data[$key]['name']         = $value->name;
                        $data[$key]['url']          = $value->url;
                        $data[$key]['image']        = $this->cdnSosmed.$value->image;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil data media sosial"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }
}
