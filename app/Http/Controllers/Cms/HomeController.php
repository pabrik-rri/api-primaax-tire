<?php

namespace App\Http\Controllers\Cms;

use App\Models\About;
use App\Models\Product;
use App\Models\News;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class HomeController extends BaseController
{
    public function __construct()
    {
        $this->cdnProduct   = config('app.cdn')."products/";
        $this->cdnAbout     = config('app.cdn')."about/";
        $this->cdnNews      = config('app.cdn')."news/";

        $this->productModel = new Product;
        $this->newsModel    = new News;
    }

    public function product()
    {
        try {
            $product = $this->productModel->getProductNew();

            if($product)
            {
                $data   = array();

                if(sizeof($product) > 0)
                {
                    foreach ($product as $key => $value) {

                        $now                        = date('Y-m-d');
                        $inputDate                  = date('Y-m-d', strtotime($value->created_at));

                        $data[$key]['id']           = $value->id;
                        $data[$key]['name']         = $value->name;
                        $data[$key]['size']         = "Tersedia ". $this->productModel->getCountCategory($value->product_category_id)." ukuran";
                        $data[$key]['image']        = $this->cdnProduct.$value->image;
                        $data[$key]['is_new']       = $inputDate != $now ? 0 : 1;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil data produk best seller"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }

    public function news()
    {
        try {
            $news        = News::where('status',1)
                                ->orderBy('created_at','DESC')
                                ->limit(6)
                                ->get();

            if($news)
            {
                $data   = array();

                if(sizeof($news) > 0)
                {
                    foreach ($news as $key => $value) {
                        $data[$key]['id']           = $value->id;
                        $data[$key]['title']        = $value->title;
                        $data[$key]['description']  = $value->description;
                        $data[$key]['user_created'] = $value->User->name;
                        $data[$key]['date_created'] = date('Y-m-d H:i:s', strtotime($value->created_at));
                        $data[$key]['image']        = $this->cdnNews.$value->image;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil data berita"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }

    public function about()
    {
        try {
            $about        = About::where('status',1)
                                ->orderBy('created_at','DESC')
                                ->limit(6)
                                ->get();

            if($about)
            {
                $data   = array();

                if(sizeof($about) > 0)
                {
                    foreach ($about as $key => $value) {
                        $data[$key]['id']           = $value->id;
                        $data[$key]['title']        = $value->title;
                        $data[$key]['description']  = $value->description;
                        $data[$key]['image']        = $this->cdnAbout.$value->image;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil data tentang kami"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }
}
