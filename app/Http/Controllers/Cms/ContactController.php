<?php

namespace App\Http\Controllers\Cms;

use App\Models\Contact;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class ContactController extends BaseController
{
    public function index(Request $request)
    {
        try {
            $contact                = new Contact;
            $contact->first_name    = $request->first_name;
            $contact->last_name     = $request->last_name;
            $contact->email         = $request->email;
            $contact->message       = $request->message;

            if($contact->save())
            {
                return response()->json([
                    'meta' => ['code' => 200, 'message' => "Berhasil mengirimkan pesan"],
                    'data' => $contact
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengirimkan pesan"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }
}
