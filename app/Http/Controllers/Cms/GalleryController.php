<?php

namespace App\Http\Controllers\Cms;

use App\Models\Gallery;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class GalleryController extends BaseController
{
    public function __construct(Request $request)
    {
        $this->cdnGallery = config('app.cdn')."galeries/";
    }

    public function index(Request $request)
    {
        try {
            $gallery        = Gallery::where('status',1)
                                ->orderBy('id','DESC')
                                ->limit(12)
                                ->get();

            if($gallery)
            {
                $data   = array();

                if(sizeof($gallery) > 0)
                {
                    foreach ($gallery as $key => $value) {
                        $data[$key]['id']           = $value->id;
                        $data[$key]['title']        = $value->title;
                        $data[$key]['description']  = $value->description;
                        $data[$key]['image']        = $this->cdnGallery.$value->image;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil data galeri"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }
}
