<?php

namespace App\Http\Controllers\Cms;

use App\Models\City;
use Laravel\Lumen\Routing\Controller as BaseController;

class CityController extends BaseController
{
    public function __construct()
    {
        $this->modelCities = new City;
    }

    public function index()
    {
        try {
            $cities        = $this->modelCities->getAllData();

            if($cities)
            {
                $data   = array();

                if(sizeof($cities) > 0)
                {
                    foreach ($cities as $key => $value) {
                        $data[$key]['id']          = $value->id;
                        $data[$key]['city']        = $value->city;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil data kota"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }
}
