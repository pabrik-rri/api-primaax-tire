<?php

namespace App\Http\Controllers\Cms;

use App\Models\Slider;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class SliderController extends BaseController
{
    public function __construct(Request $request)
    {
        $this->cdnSlider = config('app.cdn')."sliders/";
    }

    public function index(Request $request)
    {
        try {
            $slider        = Slider::where('status',1)
                                ->orderBy('id','DESC')->get();

            if($slider)
            {
                $data   = array();

                if(sizeof($slider) > 0)
                {
                    foreach ($slider as $key => $value) {
                        $data[$key]['id']           = $value->id;
                        $data[$key]['title']        = $value->title;
                        $data[$key]['description']  = $value->description;
                        $data[$key]['image']        = $this->cdnSlider.$value->image;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil data slider"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }
}
