<?php

namespace App\Http\Controllers\Cms;

use App\Models\Store;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class PartnerController extends BaseController
{
    public function __construct()
    {
        $this->modelPartners = new Store;
    }

    public function index(Request $request)
    {
        try {

            $latitude   = isset($request->latitude) ? $request->latitude : null;
            $longitude  = isset($request->longitude) ? $request->longitude : null;
            $city       = isset($request->city) ? $request->city : null;
            $store      = isset($request->store) ? $request->store : null;

            $partners   = $this->modelPartners->getDataPartner($latitude, $longitude, $city, $store);

            if($partners)
            {
                $data   = array();

                if(sizeof($partners) > 0)
                {
                    foreach ($partners as $key => $value) {
                        $data[$key]['id']           = $value->id;
                        $data[$key]['name']         = $value->name;
                        $data[$key]['latitude']     = $value->latitude;
                        $data[$key]['longitude']    = $value->longitude;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil data partner"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }
}
