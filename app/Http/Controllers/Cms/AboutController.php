<?php

namespace App\Http\Controllers\Cms;

use App\Models\About;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class AboutController extends BaseController
{
    public function __construct()
    {
        $this->cdnAbout = config('app.cdn')."about/";
    }

    public function index()
    {
        try {
            $news        = About::where('status',1)
                                ->orderBy('created_at','DESC')->get();

            if($news)
            {
                $data   = array();

                if(sizeof($news) > 0)
                {
                    foreach ($news as $key => $value) {
                        $data[$key]['id']           = $value->id;
                        $data[$key]['title']        = $value->title;
                        $data[$key]['description']  = $value->description;
                        $data[$key]['image']        = $this->cdnAbout.$value->image;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil data tentang kami"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }

    public function show($id)
    {
        try {
            
            $about        = About::find($id);

            if($about)
            {
                $data['id']           = $about->id;
                $data['title']        = $about->title;
                $data['description']  = $about->description;
                $data['image']        = $this->cdnAbout.$about->image;

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil detail tentang kami"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }
}
