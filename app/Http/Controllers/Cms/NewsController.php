<?php

namespace App\Http\Controllers\Cms;

use App\Models\News;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class NewsController extends BaseController
{
    public function __construct(Request $request)
    {
        $this->cdnNews      = config('app.cdn')."news/";
        $this->modelNews    = new News;
        $this->limit        = 25;
    }

    public function index(Request $request)
    {
        try {
            
            $offset      = isset($request->page) ? $request->page * $this->limit : 0;
            $news        = $this->modelNews->getDataWithPaginate($this->limit, $offset);

            if($news)
            {
                $data   = array();

                if(sizeof($news) > 0)
                {
                    foreach ($news as $key => $value) {
                        $data[$key]['id']           = $value->id;
                        $data[$key]['title']        = $value->title;
                        $data[$key]['description']  = $value->description;
                        $data[$key]['user_created'] = $value->User->name;
                        $data[$key]['date_created'] = date('Y-m-d H:i:s', strtotime($value->created_at));
                        $data[$key]['image']        = $this->cdnNews.$value->image;
                    }
                }

                //set data page
                $pages          = $this->modelNews->getCountData() / $this->limit;

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data,
                    'pages' => ceil($pages)
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil data berita"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }

    public function show($id)
    {
        try {
            
            $news        = News::find($id);

            if($news)
            {
                $data['id']           = $news->id;
                $data['title']        = $news->title;
                $data['description']  = $news->description;
                $data['user_created'] = $news->User->name;
                $data['date_created'] = date('Y-m-d H:i:s', strtotime($news->created_at));
                $data['image']        = $this->cdnNews.$news->image;

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal mengambil detail berita"]
            ]);
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." line : ". $e->getLine() ]
            ]);
        }
    }
}
