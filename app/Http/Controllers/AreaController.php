<?php

namespace App\Http\Controllers;

use App\Models\Province;
use App\Models\City;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

class AreaController extends BaseController
{
    public function province(Request $request)
    {
        $province        = Province::orderBy('province','ASC')->get();

        if($province)
        {
            $data   = array();

            if(sizeof($province) > 0)
            {
                foreach ($province as $key => $value) {
                    $data[$key]['id_propinsi']     = $value->id;
                    $data[$key]['nama_propinsi']   = $value->province;
                }
            }

            return response()->json([
                'meta' => ['code' => 200, 'message' => "success"],
                'data' => $data
            ]);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "Gagal mengambil data provinsi"]
        ]);
    }

    public function city(Request $request)
    {
        $city        = City::orderBy('province_id','ASC')
                        ->where('province_id', $request->province)
                        ->get();

        if($city)
        {
            $data   = array();

            if(sizeof($city) > 0)
            {
                foreach ($city as $key => $value) {
                    $data[$key]['id_kabkot']       = $value->id;
                    $data[$key]['id_propinsi']     = $value->province_id;
                    $data[$key]['nama_kabkot']     = $value->city;
                }
            }

            return response()->json([
                'meta' => ['code' => 200, 'message' => "success"],
                'data' => $data
            ]);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "Gagal mengambil data kota"]
        ]);
    }
}
