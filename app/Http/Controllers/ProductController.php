<?php

namespace App\Http\Controllers;

use App\Models\Mitra;
use App\Models\Product;
use App\Models\ProductMitra;
use App\Models\ProductRegion;
use App\Models\ProductCategory;
use App\Models\Category;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

class ProductController extends BaseController
{
    public function __construct(Request $request)
    {
        $this->cdn = config('app.cdn');

        if($request->has('token')){
            $this->token       = $request->token;
            $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

            $this->user        = Mitra::find($this->credentials->sub);
        }

        $this->limit            = 24;
        $this->modelProducts    = new Product;
    }

    /**
	 * [index description]
	 * @return [type] [description]
	 */
	public function index( Request $request )
	{
        $offset         = isset($request->page) ? $request->page*$this->limit : 0;

        $products       = $this->modelProducts->getDataWithPaginate($this->limit, $offset, $request->product_category_id);
		$subcategory    = ProductCategory::find($request->product_category_id);

		$data = array();

		$images = $this->cdn.'/no_image_available.png';

		if($subcategory) {
			$images = $this->cdn.'/banners/'. $subcategory->image;
		}

		if($products)
		{
			foreach ($products as $key => $product) {
                $now                        = date('Y-m-d');
                $inputDate                  = date('Y-m-d', strtotime($product->created_at));

                $data[$key]['id']       		= $product->id;
                $data[$key]['code']     		= $product->code;
                $data[$key]['name']     		= $product->name;
                $data[$key]['description']     	= $product->description;
                $data[$key]['price']    		= $product->price_market;
                $data[$key]['size']             = "Tersedia ". $this->modelProducts->getCountCategory($product->product_category_id)." ukuran";
                $data[$key]['image']    		= $product->image == ""? $this->cdn.'/no_image_available.png' : $this->cdn."/products/".$product->image;
                $data[$key]['is_new']           = $inputDate != $now ? 0 : 1;
			}
        }
        
        //set pages
        $pages = $this->modelProducts->getCountData($request->product_category_id) / $this->limit;
        $pages = ceil($pages);

		return response()->json( array( 'message' => 'success', 'banner' => $images, 'data' => $data, 'pages' => $pages ), 200 );
	}

    /**
	 * [index description]
	 * @return [type] [description]
	 */
	public function productPO( Request $request )
	{
        $products       = Product::where('product_category_id', $request->product_category_id)->get();
        $productMitra   = array();

        if(isset($this->user)){
            $productMitra   = ProductMitra::where('mitra_id', $this->user->id)->pluck('product_id')->toArray();
        }

		if($request->city != "" && $request->city != "Bandung" && $request->city != "Kota Bandung" && $request->city != "Kab Bandung" && $request->city != "bandung") {
            $products       = ProductRegion::select([
                                    'products.id as id',
                                    'products.code as code',
                                    'products.name as name',
                                    'products.description as description',
                                    'product_regions.price_warehouse as price_warehouse',
                                    'products.image as image',
                                ])
                                ->join('products','product_regions.product_id', '=' ,'products.id')
                                ->join('cities','product_regions.city_id', '=' ,'cities.id')
                                ->where('products.product_category_id', $request->product_category_id)
                                ->where('cities.city', 'like', '%'. $request->city .'%')
                                ->get();

            if(count($products) == 0) {
                $products       = Product::where('product_category_id', $request->product_category_id)->get();
            }
        }

		$subcategory    = ProductCategory::find($request->product_category_id);

		$data = array();

		$images = $this->cdn.'/no_image_available.png';

		if($subcategory) {
			$images = $this->cdn.'/banners/'. $subcategory->image;
		}

		if($products)
		{
			foreach ($products as $key => $product) {
                $data[$key]['id']       		= $product->id;
                $data[$key]['code']     		= $product->code;
                $data[$key]['name']     		= $product->name;
                $data[$key]['description']     	= $product->description;
                $data[$key]['price']    		= $product->price_warehouse;
                $data[$key]['image']    		= $product->image == ""? $this->cdn.'/no_image_available.png' : $this->cdn."/products/".$product->image;

                if(in_array($product->id, $productMitra)){
                    $myStock = ProductMitra::where('product_id', $product->id)
                                    ->where('mitra_id', $this->user->id)->first();

                    $data[$key]['is_stock']     = true;
                    $data[$key]['my_stock']     = $myStock->qty;
                } else {
                    $data[$key]['is_stock']     = false;
                    $data[$key]['my_stock']     = 0;
                }
			}
		}

		return response()->json( array( 'message' => 'success', 'banner' => $images, 'data' => $data ), 200 );
	}

	/**
	 * [categories description]
	 * @return [type] [description]
	 */
    public function categories()
	{
		$categories = Category::all();

		$data = array();
		foreach ($categories as $key => $category) {

			$images = $category->banner == "" ? $this->cdn.'/no_image_available.png' : $images = $this->cdn.'/categories/'. $category->banner;

            $data[$key]['id']       = $category->id;
            $data[$key]['name']     = $category->name;
            $data[$key]['image']    = $images;
		}

		return response()->json( array( 'message' => 'success', 'data' => $data ), 200 );

	}

	/**
	 * [productCategory description]
	 * @return [type] [description]
	 */
	public function productCategory( Request $request )
	{
        $offset     = isset($request->page) ? $request->page*$this->limit : 0;

        $categories = ProductCategory::orderBy('name','ASC')
                        ->offset($offset)
                        ->limit($this->limit)
                        ->get();
        
        if(!empty($request->category_id)){
            $categories = ProductCategory::where('category_id', $request->category_id)
                ->offset($offset)
                ->limit($this->limit)
                ->get();
        }

        $data = array();

		if ($categories) {
            if(count($categories) > 0)
            {
                foreach ($categories as $key => $category) {

                    $product = Product::where('product_category_id', $category->id)->first();

                    if($product){
                        $image_web  = $this->cdn."/products/".$product->image;
                        $images     = $category->image == "" ? $this->cdn.'/no_image_available.png' : $this->cdn."/banners/".$category->image;
                    }else{
                        $image_web  = $this->cdn.'/no_image_available.png';
                        $images     = $this->cdn.'/no_image_available.png';
                    }

                    $data[$key]['id']           = $category->id;
                    $data[$key]['name']         = $category->name;
                    $data[$key]['code']         = $category->name;
                    $data[$key]['image']        = $images;
                    $data[$key]['image_web']    = $image_web;
                    $data[$key]['size']         = "";
                    $data[$key]['price']        = 0;
                    $data[$key]['is_new']       = 0;
                    $data[$key]['description']  = "";
                }
            }
        }
        
        //set pages
        $pages = sizeof($categories) / $this->limit;
        $pages = ceil($pages);

		return response()->json( array( 'message' => 'success', 'data' => $data, 'pages' => $pages ), 200 );
	}
}
