<?php

namespace App\Http\Controllers\Mitra;

use App\User;
use App\Models\Mitra;
use App\Models\Store;
use App\Models\Faq;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

class FaqController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token       = $request->token;
        $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

        $this->profile     = User::find($this->credentials->sub);

        if($this->profile->level_id == 3) {
            $this->user        = Mitra::find($this->profile->mitra_id);
        }

        if($this->profile->level_id == 4) {
            $this->user        = Store::find($this->profile->mitra_id);
        }
    }

    public function index(Request $request)
    {
        $faq        = Faq::where('role','mitra')->get();

        if($faq)
        {
            return response()->json([
                'meta' => ['code' => 200, 'message' => "success"],
                'data' => $faq
            ], 200);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "Gagal mengambil data faq"]
        ], 500);
    }

    public function detail(Request $request, $id)
    {
        $faq        = Faq::find($id);

        if($faq)
        {
            return response()->json([
                'meta' => ['code' => 200, 'message' => "success"],
                'data' => $faq
            ],200);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "Gagal mengambil data detail faq"]
        ], 500);
    }
}
