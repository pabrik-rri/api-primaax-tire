<?php

namespace App\Http\Controllers\Mitra;

use App\User;
use App\Models\Mitra;
use App\Models\Store;
use App\Models\Customer;
use App\Models\Order;
use App\Models\OrderDetail;
use App\Models\OrderHistory;
use App\Models\Product;
use App\Models\ProductMitra;
use App\Models\Receivables;
use App\Models\Radius;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class OrderController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token       = $request->token;
        $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

        $this->profile     = User::find($this->credentials->sub);

        if($this->profile->level_id == 3) {
            $this->user        = Mitra::find($this->profile->mitra_id);
        }

        if($this->profile->level_id == 4) {
            $this->user        = Store::find($this->profile->mitra_id);
        }

        $this->cdn         = config('app.cdn');
        $this->cdnCustomer = $this->cdn."/avatars/customer/";

        $this->objOrder    = new Order;
        $this->notif       = app('App\Http\Controllers\NotificationController');
    }

    public function index(Request $request)
    {
        try{
            $id         = $this->user->id;
            $is_courier = 0;

            if($this->user->is_courier == 1) { 
                $id         = $this->user->mitra_id; 
                $is_courier = 1;
            }
            $role = $this->profile->level_id == 3 ? "mitra" : "store";

            $order        = $this->objOrder->getDataOrder($id, $role, $is_courier);

            if($order)
            {
                $data = array();

                if(count($order) > 0)
                {
                    foreach ($order as $key => $value)
                    {
                        switch ($value->status) {
                            case '0':
                                $status = "Pending";
                                break;

                            case '1':
                                $status = "Diterima";
                                break;

                            case '2':
                                $status = "Persiapan Pengiriman";
                                break;

                            case '3':
                                $status = "Dalam Pengiriman";
                                break;

                            case '4':
                                $status = "Batal";
                                break;

                            case '5':
                                $status = "Selesai";
                                break;

                            case '6':
                                $status = "Pindahan";
                                break;

                            default:
                                $status = "";
                                break;
                        }

                        $image = "";

                        if ($value->is_po == 1) {
                            $image = $this->cdnCustomer."/pre-order-profile.png";
                        } else {
                            if(isset($value->Customer) && $value->Customer->avatar != "") {
                                $image = $this->cdnCustomer.$value->Customer->avatar;
                            }
                        }

                        $data[$key]['orders_id']              = $value->order_id;
                        $data[$key]['order_code']             = $value->order_code;
                        $data[$key]['fulname']                = isset($value->Mitra) ? $value->Mitra->name : "";
                        $data[$key]['name_store']             = isset($value->Mitra) ? $value->Mitra->name_store : "";
                        $data[$key]['address']                = $value->address;
                        $data[$key]['mitra_address']          = isset($value->Mitra) ? $value->Mitra->address : "";
                        $data[$key]['mitra_latitude']         = isset($value->Mitra) ? $value->Mitra->latitude : "";
                        $data[$key]['mitra_longitude']        = isset($value->Mitra) ? $value->Mitra->longitude : "";
                        $data[$key]['latitude']               = $value->latitude;
                        $data[$key]['longitude']              = $value->longitude;
                        $data[$key]['courier_id']             = $value->courier_id;
                        $data[$key]['address_note']           = $value->address_note;
                        $data[$key]['courier_name']           = isset($value->Courier) ? $value->Courier->name : "";
                        $data[$key]['customer_name']          = isset($value->Customer) ? $value->Customer->name : "";

                        if($value->is_po == 1) {
                            $data[$key]['customer_name']      = "Pre Order Barang";
                        }

                        $data[$key]['customer_phone']         = isset($value->Customer) ? $value->Customer->mobile : "";
                        $data[$key]['customer_address']       = $value->address;
                        $data[$key]['customer_latitude']      = $value->latitude;
                        $data[$key]['customer_longitude']     = $value->longitude;
                        $data[$key]['destination_address']    = $value->address;
                        $data[$key]['destination_latitude']   = $value->latitude;
                        $data[$key]['destination_longitude']  = $value->longitude;
                        $data[$key]['customer_avatar']        = "";
                        $data[$key]['payment']                = $value->payment;
                        $data[$key]['status_payment']         = $value->status_payment;
                        $data[$key]['reviews']                = $value->review;
                        $data[$key]['stars']                  = $value->star;
                        $data[$key]['status']                 = $status;
                        $data[$key]['discount']               = $value->discount;
                        $data[$key]['ongkir']                 = $value->ongkir;
                        $data[$key]['total_price']            = $value->total_price;
                        $data[$key]['delivery_date']          = $value->delivery_date;
                        $data[$key]['is_po']                  = $value->is_po;
                        $data[$key]['order_date']             = date_format(date_create($value->order_date),'Y-m-d H:i:s');
                        $data[$key]['finish_date']            = date_format(date_create($value->finish_date),'Y-m-d H:i:s');
                        $data[$key]['image']                  = $image;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }
        } catch(\Exception $e)
        {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage(), 'line' => $e->getLine()]
            ]);
        }
    }

    public function history(Request $request)
    {
        try {
            $id         = $this->user->id;
            $is_courier = 0;

            if($this->user->is_courier == 1) { 
                $id         = $this->user->mitra_id; 
                $is_courier = 1;
            }
            $role = $this->profile->level_id == 3 ? "mitra" : "store";

            $order        = $this->objOrder->getDataHistory($id, $role, $is_courier);

            if($order)
            {
                $data = array();

                if(count($order) > 0)
                {
                    foreach ($order as $key => $value) {

                        switch ($value->status) {
                            case '0':
                                $status = "Pending";
                                break;

                            case '1':
                                $status = "Diterima";
                                break;

                            case '2':
                                $status = "Persiapan Pengiriman";
                                break;

                            case '3':
                                $status = "Dalam Pengiriman";
                                break;

                            case '4':
                                $status = "Batal";
                                break;

                            case '5':
                                $status = "Selesai";
                                break;

                            case '6':
                                $status = "Pindahan";
                                break;

                            default:
                                $status = "";
                                break;
                        }

                        $image = "";


                        if ($value->is_po == 1) {
                            $image = $this->cdnCustomer."/pre-order-profile.png";
                        } else {
                            if(isset($value->Customer) && $value->Customer->avatar != "") {
                                $image = $this->cdnCustomer.$value->Customer->avatar;
                            }
                        }

                        $data[$key]['orders_id']              = $value->order_id;
                        $data[$key]['order_code']             = $value->order_code;
                        $data[$key]['fulname']                = isset($value->Mitra) ? $value->Mitra->name : "";
                        $data[$key]['name_store']             = isset($value->Mitra) ? $value->Mitra->name_store : "";
                        $data[$key]['address']                = $value->address;
                        $data[$key]['mitra_address']          = isset($value->Mitra) ? $value->Mitra->address : "";
                        $data[$key]['mitra_latitude']         = isset($value->Mitra) ? $value->Mitra->latitude : "";
                        $data[$key]['mitra_longitude']        = isset($value->Mitra) ? $value->Mitra->longitude : "";
                        $data[$key]['latitude']               = $value->latitude;
                        $data[$key]['longitude']              = $value->longitude;
                        $data[$key]['address_note']           = $value->address_note;
                        $data[$key]['courier_id']             = $value->courier_id;
                        $data[$key]['courier_name']           = isset($value->Courier) ? $value->Courier->name : "";
                        $data[$key]['customer_name']          = isset($value->Customer) ? $value->Customer->name : "";

                        if($value->is_po == 1) {
                            $data[$key]['customer_name']      = "Pre Order Barang";
                        }

                        $data[$key]['customer_phone']         = isset($value->Customer) ? $value->Customer->mobile : "";
                        $data[$key]['customer_address']       = $value->address;
                        $data[$key]['customer_latitude']      = $value->latitude;
                        $data[$key]['customer_longitude']     = $value->longitude;
                        $data[$key]['destination_address']    = $value->address;
                        $data[$key]['destination_latitude']   = $value->latitude;
                        $data[$key]['destination_longitude']  = $value->longitude;
                        $data[$key]['customer_avatar']        = "";
                        $data[$key]['payment']                = $value->payment;
                        $data[$key]['status_payment']         = $value->status_payment;
                        $data[$key]['reviews']                = $value->review;
                        $data[$key]['stars']                  = $value->star;
                        $data[$key]['status']                 = $status;
                        $data[$key]['discount']               = $value->discount;
                        $data[$key]['ongkir']                 = $value->ongkir;
                        $data[$key]['total_price']            = $value->total_price;
                        $data[$key]['delivery_date']          = $value->delivery_date;
                        $data[$key]['order_date']             = date_format(date_create($value->order_date),'Y-m-d H:i:s');
                        $data[$key]['finish_date']            = date_format(date_create($value->finish_date),'Y-m-d H:i:s');
                        $data[$key]['image']                  = $image;
                        $data[$key]['is_po']                  = $value->is_po;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage(), 'line' => $e->getLine()]
            ]);
        }
    }

    public function detail($id)
    {
        try{
            $order        = $this->objOrder->getDataDetail($id);

            if($order)
            {
                switch ($order->status_order) {
                    case '0':
                        $status = "Pending";
                        break;

                    case '1':
                        $status = "Diterima";
                        break;

                    case '2':
                        $status = "Persiapan Pengiriman";
                        break;

                    case '3':
                        $status = "Dalam Pengiriman";
                        break;

                    case '4':
                        $status = "Batal";
                        break;

                    case '5':
                        $status = "Selesai";
                        break;

                    case '6':
                        $status = "Pindahan";
                        break;

                    default:
                        $status = "";
                        break;
                }

                $image = "";


                if ($order->is_po == 1) {
                    $image = $this->cdnCustomer."/pre-order-profile.png";
                } else {
                    if(isset($order->Customer) && $order->Customer->avatar != "") {
                        $image = $this->cdnCustomer.$order->Customer->avatar;
                    }
                }

                $data['orders']['orders_id']              = $order->order_id;
                $data['orders']['order_code']             = $order->order_code;
                $data['orders']['fulname']                = isset($order->Mitra) ? $order->Mitra->name : "";
                $data['orders']['name_store']             = isset($order->Mitra) ? $order->Mitra->name_store : "";
                $data['orders']['address']                = $order->address;
                $data['orders']['mitra_address']          = isset($order->Mitra) ? $order->Mitra->address : "";
                $data['orders']['mitra_latitude']         = isset($order->Mitra) ? $order->Mitra->latitude : "";
                $data['orders']['mitra_longitude']        = isset($order->Mitra) ? $order->Mitra->longitude : "";
                $data['orders']['store_phone']            = isset($order->Mitra) ? $order->Mitra->mobile : "";
                $data['orders']['latitude']               = $order->latitude;
                $data['orders']['longitude']              = $order->longitude;
                $data['orders']['address_note']           = $order->address_note;
                $data['orders']['courier_id']             = $order->courier_id;
                $data['orders']['courier_name']           = isset($order->Courier) ? $order->Courier->name : "";
                $data['orders']['customer_name']          = isset($order->Customer) ? $order->Customer->name : "";

                if($order->is_po == 1) {
                    $data['orders']['customer_name']      = "Pre Order Barang";
                }

                $data['orders']['customer_phone']         = isset($order->Customer) ? $order->Customer->mobile : "";
                $data['orders']['customer_mobile']        = isset($order->Customer) ? $order->Customer->mobile : "";
                $data['orders']['customer_address']       = $order->address;
                $data['orders']['customer_latitude']      = $order->latitude;
                $data['orders']['customer_longitude']     = $order->longitude;
                $data['orders']['customer_avatar']        = "";
                $data['orders']['payment']                = $order->payment;
                $data['orders']['status_payment']         = $order->status_payment;
                $data['orders']['reviews']                = $order->review;
                $data['orders']['stars']                  = $order->star;
                $data['orders']['status']                 = $status;
                $data['orders']['discount']               = $order->discount;
                $data['orders']['ongkir']                 = $order->ongkir;
                $data['orders']['total_price']            = $order->total_price;
                $data['orders']['delivery_date']          = $order->delivery_date;
                $data['orders']['order_date']             = date_format(date_create($order->order_date),'Y-m-d H:i:s');
                $data['orders']['finish_date']            = date_format(date_create($order->finish_date),'Y-m-d H:i:s');
                $data['orders']['image']                  = $image;
                $data['orders']['is_po']                  = $order->is_po;

                //detail
                $orderDetail                    = OrderDetail::where('order_id', $id)->get();

                foreach ($orderDetail as $key => $value) {
                    $data['detail'][$key]['id']           = $value->id;
                    $data['detail'][$key]['name']         = isset($value->Product) ? $value->Product->name : "";
                    $data['detail'][$key]['qty']          = $value->qty;
                    $data['detail'][$key]['price']        = $order->is_po == 1 ? $value->Product->price_warehouse : $value->Product->price_market;
                    $data['detail'][$key]['subtotal']     = $order->is_po == 1 ? ($value->qty * $value->Product->price_warehouse) : ($value->qty * $value->Product->price_market);
                    $data['detail'][$key]['note']         = $value->note;
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => $data
                ]);
            }
        } catch(\Exception $e){
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage(), 'line' => $e->getLine()]
            ]);
        }


    }

    public function isAdjustmentPO(Request $request, $id)
    {
        $order = Order::find($id);

        if($order)
        {
            if($request->is_adjustment == 1)
            {
                $order->status = 5;
                $order->save();

                $detail = OrderDetail::where('order_id', $id)->get();

                // return $detail; exit;

                foreach ($detail as $key => $value) {
                    $duplicate                      = ProductMitra::where('mitra_id', $order->mitra_id)->where('product_id', $value->product_id)->first();

                    $product_mitras                 = isset($duplicate) ? ProductMitra::where('mitra_id', $order->mitra_id)->where('product_id', $value->product_id)->first() : new ProductMitra;
                    $product_mitras->product_id     = $value->product_id;
                    $product_mitras->qty            = isset($duplicate) ? $duplicate->qty + $value->qty : $value->qty;
                    $product_mitras->min_qty        = 0;
                    $product_mitras->price          = $value->price;
                    $product_mitras->mitra_id       = $order->mitra_id;
                    $product_mitras->save();
                }

                $meta = [ 'code' => 200, 'message' => 'Berhasil melakukan penyesuaian barang PO'];
            } else {
                $order->status = 4;
                $order->note   = $request->note;
                $order->save();

                $meta = [ 'code' => 200, 'message' => 'Berhasil melakukan penolakan ketidaksesuaian barang PO'];
            }

            return response()->json( array( 'meta' => $meta), 200 );
        }
    }

    public function isAccept(Request $request, $id)
    {
        $orders     = Order::find($id);
        $is_stock   = 2;

        if($this->user->is_courier == 1) {
            $orders->courier_id = $this->user->id;
            $mitraId = $this->user->mitra_id;
        } else {
            $orders->courier_id = $request->courier_id;
            $mitraId = $this->user->id;
        }

        if ($orders) {

            if($request->is_accept == 1)
            {
                if($orders->status == 0 || $orders->status == 6)
                {
                    \DB::beginTransaction();
                        //checkstock
                        $order_details  = OrderDetail::where('order_id', $orders->id)->get();
                        $product_mitras = ProductMitra::where('mitra_id', $mitraId)->pluck('product_id')->toArray();

                        $is_full_stock  = count($order_details);
                        $is_sold_out    = 0;
                        $product_ex     = array();
                        
                        if(count($product_mitras) > 0)
                        {
                            foreach ($order_details as $key => $value) {

                                if(in_array($value->product_id, $product_mitras))
                                {
                                    $product_out    = ProductMitra::where('mitra_id', $mitraId)
                                                        ->where('product_id',$value->product_id)->first();
            
                                    if($value->qty < $product_out->qty) {
                                        $product_out->qty = $product_out->qty - $value->qty;
                                        $product_out->save();
                                        
                                        $is_sold_out    = 1;
                                    } 

                                    elseif(($value->qty > $product_out->qty || $value->qty == $product_out->qty) && $product_out->qty != 0) {
                                        $order_out      = OrderDetail::find($value->id);
                                        $order_out->qty = $product_out->qty;
                                        $order_out->save();
            
                                        $product_out->qty = $product_out->qty - $product_out->qty;
                                        $product_out->save();

                                        $is_full_stock  = $is_full_stock - 1;
                                        $is_sold_out    = 1;
                                    } 

                                    elseif($product_out->qty == 0) {
                                        array_push($product_ex, $value->id);
                                        $is_full_stock  = $is_full_stock - 1;
                                    }

                                } 

                                else {
                                    array_push($product_ex, $value->id);
                                    $is_full_stock  = $is_full_stock - 1;
                                }
                            }

                        } else {
                            $is_stock = 0;
                        }

                        if($is_full_stock != count($order_details) && $is_sold_out == 1) 
                        { 
                            $is_stock = 1;

                            //clear order
                            for($i=0; $i < count($product_ex); $i++) {
                                $order_out      = OrderDetail::find($product_ex[$i]);
                                $order_out->qty = 0;
                                $order_out->save();
                            }
                        } 
                        elseif($is_full_stock == 0)
                        {
                            $is_stock = 0;
                        }
                    
                        if($is_stock == 2) {
                            $orders->status = 1;
                            $orders->update();

                            //sendnotif accept
                            $customer     = Customer::find($orders->user_id);
                            $mitra        = Mitra::find($orders->mitra_id);

                            $token_fcm    = $customer->token_fcm;
                            $title        = "Order Anda sedang dipersiapkan oleh : ". $mitra->name;
                            $description  = "Toko anda telah menerima pesanan baru";
                            $type         = "Order";
                            
                            $meta               = [ 'code' => 200, 'message' => 'Berhasil Melakukan Pemesanan.'];
                            $orders['is_stock'] = 1;

                            $this->notif->sendAccept($token_fcm, $title, $description, $type, $orders->user_id, $orders->id, $orders->mitra_id);
                        } 
                        elseif($is_stock == 1) {

                            $orders->status = 1;
                            $orders->update();
                            
                            $customer     = Customer::find($orders->user_id);
                            $mitra        = Mitra::find($orders->mitra_id);

                            $meta   = [ 'code' => 200, 'message' => 'Stok Menipis, segera melakukan PO untuk barang yang sudah menipis'];
                            $orders['is_stock'] = 1;

                            //sendnotif accept
                            $customer     = Customer::find($orders->user_id);
                            $mitra        = Mitra::find($orders->mitra_id);

                            $token_fcm    = $customer->token_fcm;
                            $title        = "Order Anda sedang dipersiapkan oleh : ". $mitra->name;
                            $description  = "Toko anda telah menerima pesanan baru";
                            $type         = "Order";

                            $this->notif->sendAccept($token_fcm, $title, $description, $type, $orders->user_id, $orders->id, $orders->mitra_id);
                        }
                        else {
                            $meta               = [ 'code' => 200, 'message' => 'Stok Barang yang dipesan tidak tersedia, mohon melakukan pre order.'];
                            $orders['is_stock'] = 0;
                        }
                    \DB::commit();  
                } else {
                    $meta = [ 'code' => 200, 'message' => 'Order ini sudah diterima, silahkan melakukan refresh pada list orderan Anda' ];
                } 
            }
            else
            {
                $customer     = Customer::find($orders->user_id);
                $mitra        = Mitra::find($orders->mitra_id);

                if(app('session')->has('mitra_id'))
                {
                    $except      = app('session')->get('mitra_id');
                } else {
                    $except      = array();
                }

                $latitude           = $orders->latitude;
                $longitude          = $orders->longitude;
                $distance           = Radius::where('is_radius_reject',1)->first();
                $distance_radius    = $distance->radius;

                array_push($except, $orders->mitra_id);

                $mitra_id   = $this->objOrder->getByDistance($latitude,$longitude,$distance_radius,$except);

                if($mitra_id != "" || $mitra_id != NULL)
                {
                    $orders->status     = 6;
                    $orders->mitra_id   = $mitra_id;
                    $orders->courier_id = "";
                    $orders->save();

                    $mitra        = Mitra::find($mitra_id);
                    $token_fcm    = $mitra->token_fcm;
                    $title        = "Order Baru";
                    $description  = "Toko anda mendapatkan pesanan baru";
                    $type         = "Order";

                    $this->notif->sendNotif($token_fcm, $title, $description, $type, $mitra_id, $orders->id);
                }
                else {
                    $orders->status     = 4;
                    $orders->courier_id = $this->user->id;
                    $orders->note       = $request->note;
                    $orders->update();

                    //sendnotif reject
                    $token_fcm    = $customer->token_fcm;
                    $title        = "Order Telah di tolak oleh : ". $mitra->name;
                    $description  = "Toko ". $mitra->name_store ." telah menolak pesanan";
                    $type         = "Order";

                    $this->notif->sendReject($token_fcm, $title, $description, $type, $orders->user_id, $orders->id, $orders->mitra_id);
                }

                $meta = [ 'code' => 200, 'message' => 'Orderan berhasil dibatalkan, silahkan refresh kembali list order Anda'];
            }

            return response()->json( array( 'meta' => $meta, 'data' => $orders ), 200 );
        }

        $meta = [ 'code' => 500, 'message' => 'failed' ];

        return response()->json( array( 'meta' => $meta, 'data' => [] ), 500 );

    }

    public function isAdjustment(Request $request, $id)
    {
        $orders = Order::find($id);

        if ($orders) {
            $meta = ['code' => 200,'message' => 'success'];

            $orders->status = 3;
            $orders->update();

            //sendnotif penyesuaian
            $customer     = Customer::find($orders->user_id);
            $mitra        = Mitra::find($orders->mitra_id);

            $token_fcm    = $customer->token_fcm;
            $title        = "Order Dalam Pengiriman oleh : ". $mitra->name;
            $description  = "Toko ". $mitra->name_store ." telah melakukan pengiriman pada pesanan";
            $type         = "Order";

            $this->notif->sendAdjustment($token_fcm, $title, $description, $type, $orders->user_id, $orders->id, $orders->mitra_id);

            return response()->json( array( 'meta' => $meta, 'data' => $orders ), 200 );
        }

        $meta = [ 'code' => 500, 'message' => 'failed'];

        return response()->json( array( 'meta' => $meta, 'data' => [] ), 500 );
    }

    public function isDelivery(Request $request, $id)
    {
        $orders = Order::find($id);

        if ($orders) {
            $meta = [ 'code' => 200, 'message' => 'success'];

            $orders->status       = 5;
            $orders->note         = $request->note;
            $orders->updated_at   = date('Y-m-d H:i:s');
            $orders->update();

            //sendnotif delivery
            $customer     = Customer::find($orders->user_id);
            $mitra        = Mitra::find($orders->mitra_id);

            $token_fcm    = $customer->token_fcm;
            $title        = "Order Telah sampai, diantarkan oleh : ". $mitra->name;
            $description  = "Toko ". $mitra->name_store ." telah selesai melakukan pengiriman pada pesanan ini";
            $type         = "Order";

            $this->notif->sendDelivery($token_fcm, $title, $description, $type, $orders->user_id, $orders->id, $orders->mitra_id);

            return response()->json( array( 'meta' => $meta, 'data' => $orders ), 200 );
        }

        $meta = [ 'code' => 500, 'message' => 'failed'];

        return response()->json( array( 'meta' => $meta, 'data' => [] ), 500 );

    }

    public function update(Request $request)
    {
        $order_id   = $request->order_id;
        $detail     = $request->detail;

        if(count($detail) > 0)
        {
            for ($i=0; $i < count($detail) ; $i++)
            {
                $order_detail = OrderDetail::find($detail[$i]['id']);

                //order history
                $history                   = New OrderHistory;
                $history->order_id         = $order_id;
                $history->order_detail_id  = $detail[$i]['id'];
                $history->qty_old          = $order_detail->qty;
                $history->qty              = $detail[$i]['qty'];
                $history->user_id          = $this->user->id;
                $history->save();

                $order_detail->qty = $detail[$i]['qty'];
                $order_detail->save();
            }

            $meta = [ 'code' => 200, 'message' => 'Berhasil mengubah data orderan'];

            return response()->json( array( 'meta' => $meta), 200 );
        }

        $meta = [ 'code' => 500, 'message' => 'Gagal mengubah data orderan'];

        return response()->json( array( 'meta' => $meta), 500 );
    }

    public function income(Request $request)
    {
        try{

            $id        = $this->user->id;

            if($this->user->is_courier == 1){
                $id   = $this->user->mitra_id;
            }

            $start      = $request->start;
            $finish     = $request->finish;

            $order      = $this->objOrder->getIncomes($id, $start, $finish);

            if($order)
            {
                $data = array();

                if(count($order['data']) > 0)
                {
                    foreach ($order['data'] as $key => $value) {

                        switch ($value->status) {
                            case '0':
                                $status = "Pending";
                                break;

                            case '1':
                                $status = "Diterima";
                                break;

                            case '2':
                                $status = "Persiapan Pengiriman";
                                break;

                            case '3':
                                $status = "Dalam Pengiriman";
                                break;

                            case '4':
                                $status = "Batal";
                                break;

                            case '5':
                                $status = "Selesai";
                                break;

                            default:
                                $status = "";
                                break;
                        }

                        $image = "";

                        if(isset($value->Customer) && $value->Customer->avatar != "") {
                            $image = $this->cdnCustomer.$value->Customer->avatar;
                        }

                        $data[$key]['orders_id']              = $value->order_id;
                        $data[$key]['order_code']             = $value->order_code;
                        $data[$key]['order_date']             = date_format(date_create($value->order_date),'Y-m-d H:i:s');
                        $data[$key]['courier_name']           = isset($value->Courier) ? $value->Courier->name : "";
                        $data[$key]['customer_name']          = isset($value->Customer) ? $value->Customer->name : "";
                        $data[$key]['customer_image']         = $image;
                        $data[$key]['status']                 = $status;
                        $data[$key]['subtotal']               = $value->subtotal;
                    }
                }

                return response()->json([
                    'meta' => ['code' => 200, 'message' => "success"],
                    'data' => [
                        'incomes' => $data,
                        'total' => $order['total']->total
                    ],
                ]);
            }

        } catch(\Exception $e) {

            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage(), 'line' => $e->getLine()]
            ]);

        }
    }

    /**
     * [checkout description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function checkout(Request $request)
    {
        $params = $request->all();
        $order  = new Order;

        $limitOrder = $this->user->limit;
        $limitUse   = $this->objOrder->getLimitUse($this->user->id, $this->user->level_id);

        if($limitUse < $limitOrder)
        {
            \DB::beginTransaction();
            try {

                //check limit
                $voucherLimit = 0;

                if(isset($params['voucher_id']))
                {
                    if($params['voucher_id'] != NULL || $params['voucher_id'] != "") {
                        $voucher    = Voucher::find($params['voucher_id']);
                        $voucherLimit = Order::where('user_id', $this->user->id)->where('voucher_id', $voucher->id)->count();
                    }
                }

                if($voucherLimit == 0) {
                    $order                    = new Order;

                    if(isset($params['voucher_id'])) {
                        $voucher    = Voucher::find($params['voucher_id']);
                        if ($voucher) {
                            $order->discount          = $voucher->discount == "" ? 0 : $voucher->discount;
                            $order->voucher_id        = $voucher->id;
                        }
                    }

                    $order->order_code        = Order::getAutoNumber('PO', 1);
                    $order->mitra_id          = $this->user->id;
                    $order->user_id           = 0;
                    $order->payment           = 'bank-transfer';
                    $order->status_payment    = 'pending';
                    $order->status            = 0;
                    $order->ongkir            = 0;
                    $order->address           = $params['address'];
                    $order->address_note      = $params['address_note'];
                    $order->latitude          = $params['latitude'];
                    $order->longitude         = $params['longitude'];
                    $order->is_po             = 1;
                    $order->is_store          = $this->profile->level_id == 3 ? 0 : 1;
                    $order->upline_id         = $this->profile->level_id == 4 ? $this->user->mitra_id : "";
                    $order->save();

                    if (count($request->product_id) > 0) {
                        foreach ($request->product_id as $i => $value) {
                            if ($request->product_id[$i] != '') {
                                $product                = Product::find($request->product_id[$i]);

                                $detail                 = new OrderDetail();
                                $detail->order_id       = $order->id;
                                $detail->product_id     = $product->id;
                                $detail->qty            = $params['qty'][$i];
                                $detail->price          = $product->price_warehouse;
                                
                                if(isset($param['note'])) {
                                    $detail->note           = $params['note'][$i];
                                }

                                $detail->save();
                            }
                        }
                    }

                    $details        = array();
                    $order_details  = OrderDetail::where('order_id', $order->id)->get();

                    foreach ($order_details as $key => $detail) {
                        $details[$key]['product_id']    = $detail->product_id;
                        $details[$key]['product_name']  = $detail->product_name;
                        $details[$key]['qty']           = $detail->qty;
                        $details[$key]['price']         = $detail->price;
                        $details[$key]['note']          = $detail->note;
                    }

                    $d_voucher = null;

                    if(isset($params['voucher_id'])) {
                        $voucher    = Voucher::find($params['voucher_id']);
                        if($voucher) {
                            $d_voucher  = array(
                                'id'        => $voucher->id,
                                'code'      => $voucher->code,
                                'discount'  => $voucher->value,
                            );
                        }
                    }

                    $data = array(
                        'order_code'      => $order->order_code,
                        'user_id'         => $order->user_id,
                        'role'            => $order->role,
                        'payment'         => $order->payment,
                        'status_payment'  => $order->status_payment,
                        'status'          => $order->status == 0 ? 'pending' : '',
                        'address'         => $order->address,
                        'address_note'    => $order->address_note,
                        'ongkir'          => $order->ongkir,
                        'discount'        => $order->discount,
                        'voucher'         => $d_voucher,
                        'order_details'   => $details
                    );

                    // insert to saldo piutang awal
                    $balanceUser                    = $this->objOrder->getBalanceUser($this->user->id, $order->id);

                    if($this->profile->level_id == 3){
                        $supplier_name = User::where('level_id',2)->first()->name;
                    }elseif($this->profile->level_id == 4){
                        $supplier_name = Mitra::find($this->profile->mitra_id)->name;
                    }

                    $balance            			= new Receivables;
                    $balance->code 					= $this->objOrder->getNumberBalance();
                    $balance->invoice_date    		= date('Y-m-d');
                    $balance->supplier_name    		= $supplier_name;
                    $balance->customer_name    		= $this->user->name;
                    $balance->value_of_receivable 	= $balanceUser;
                    $balance->balance_of_receivable	= $balanceUser;
                    $balance->is_factory    		= User::where('level_id',2)->first()->level_id;
                    $balance->order_id    		    = $order->id;
                    $balance->save();

                    $result = [
                        'message' => 'success',
                        'data'    => $data
                    ];

                    // exit;
                    \DB::commit();

                    return response()->json( $result, 200 );
                }

                else {
                    \DB::rollback();

                    $result = [
                        'meta' => [
                            'code' => 404, 
                            'message' => 'Anda sudah melebihi batas penggunaan voucher'
                        ],
                    ];

                    return response()->json( $result, 404 );
                }

            } catch(\Exception $e) {
                \DB::rollback();

                $result = [
                    'message' => $e->getMessage().' '.$e->getLine(),
                ];

                return response()->json( array( (object) $result ), 500 );
            }
        } else {
            $result = [
                'meta' => [
                    'code' => 404, 
                    'message' => 'Anda sudah melebihi batas penggunaan limit, silahkan melunasi hutang Anda terlebih dahulu'
                ],
            ];

            return response()->json( $result, 404 );
        }
    }
}
