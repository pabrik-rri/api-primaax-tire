<?php

namespace App\Http\Controllers\Mitra;

use App\User;
use App\Models\Mitra;
use App\Models\Order;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

class CourierController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token       = $request->token;
        $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

        $this->profile     = User::find($this->credentials->sub);

        if($this->profile->level_id == 3) {
            $this->user        = Mitra::find($this->profile->mitra_id);
        }

        if($this->profile->level_id == 4) {
            $this->user        = Store::find($this->profile->mitra_id);
        }

        $this->cdn          = config('app.cdn');
        $this->cdnMitra     = "../../cdn/avatars/mitra/";
        $this->cdnCourier   = "../../cdn/avatars/courier/";
        $this->getMitra     = $this->cdn."/avatars/mitra/";
        $this->getCourier   = $this->cdn."/avatars/courier/";
    }

    public function index(Request $request)
    {
        $courier        = Mitra::where('mitra_id', $this->user->id)
                            ->where('status',1)
                            ->get();

        if($courier)
        {
            $data = array();

            if(sizeof($courier) > 0)
            {
                foreach ($courier as $key => $value) {

                    $images = $value->avatar == "" ? $this->cdn."no_image_available.png" : $this->getCourier.$value->avatar;

                    $data[$key]['id']           = $value->id;
                    $data[$key]['fullname']     = $value->name;
                    $data[$key]['email']        = $value->email;
                    $data[$key]['no_ktp']       = $value->no_id_card;
                    $data[$key]['mobile']       = $value->mobile;
                    $data[$key]['gender']       = $value->gender == 1 ? "Male" : "Female";
                    $data[$key]['birth_date']   = $value->birth_date;
                    $data[$key]['mitra_id']     = $value->mitra_id;
                    $data[$key]['is_courier']   = $value->is_courier;
                    $data[$key]['status']       = $value->status;
                    $data[$key]['provinsi']     = isset($value->getCity) ? $value->getCity->Province->province : "";
                    $data[$key]['provinsi_id']  = isset($value->getCity) ? $value->getCity->province_id : "";
                    $data[$key]['city']         = isset($value->getCity) ? $value->getCity->city : "";
                    $data[$key]['city_id']      = $value->city_id;
                    $data[$key]['address']      = $value->address;
                    $data[$key]['zip_code']     = $value->zip_code;
                    $data[$key]['name_store']   = $value->name_store;
                    $data[$key]['created_at']   = date_format(date_create($value->created_at), 'Y-m-d');
                    $data[$key]['updated_at']   = date_format(date_create($value->update_at), 'Y-m-d');
                    $data[$key]['image']        = $images;
                }
            }

            return response()->json([
                'meta' => ['code' => 200, 'message' => "success"],
                'data' => $data
            ]);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "Gagal mengambil data courier"]
        ]);
    }

    public function store(Request $request)
    {
        //param
        $duplicate = Mitra::where('is_courier', 1)->where('mobile', $request->mobile)->count();

        if($duplicate == 0){
            $params             = $request->all();

            $user               = new Mitra;
            $user->name         = $params['fullname'];
            $user->no_id_card   = isset($params['no_ktp']) ? $params['no_ktp'] : "";
            $user->mobile       = $params['mobile'];
            $user->gender       = $params['gender'];
            $user->birth_date   = $params['birth_date'];
            $user->mitra_id     = $this->user->id;
            $user->is_courier   = 1;
            $user->status       = 1;
            $user->password     = app('hash')->make($params['password']);

            if($user->save())
            {
                return response()->json([
                    'meta' => ['code' => 200, 'message' => "Berhasil menambahkan kurir"]
                ]);
            }

            return response()->json([
                'meta' => ['code' => 500, 'message' => "Gagal menambahkan data kurir"]
            ]);
        } else {
            return response()->json([
                'meta' => ['code' => 500, 'message' => "No handphone yang ditambahkan telah terdaftar disistem"]
            ]);
        }
    }

    public function update(Request $request, $id)
    {
        //param
        $params             = $request->all();

        try {
            $user               = Mitra::find($id);
            $user->name         = $params['fullname'];
            $user->no_id_card   = isset($params['no_ktp']) ? $params['no_ktp'] : "";
            $user->mobile       = $params['mobile'];
            $user->gender       = $params['gender'];
            $user->birth_date   = $params['birth_date'];

            //if password != null
            if(isset($params['password']))
            {
                $user->password = app('hash')->make($params['password']);
            }

            if(isset($params['name_store']))
            {
                $user->name_store = $params['name_store'];
            }

            if(isset($params['city_id']))
            {
                $user->city_id = $params['city_id'];
            }

            if(isset($params['no_ktp']))
            {
                $user->no_id_card = $params['no_ktp'];
            }

            if(isset($params['gender']))
            {
                $user->gender = $params['gender'] == "Male" ? 1 : 2;
            }

            if(isset($params['birth_date']))
            {
                $user->birth_date = $params['birth_date'];
            }

            if(isset($params['address']))
            {
                $user->address = $params['address'];
            }

            if(isset($params['longitude']))
            {
                $user->longitude = $params['longitude'];
            }

            if(isset($params['latitude']))
            {
                $user->latitude = $params['latitude'];
            }

            if(isset($params['zip_code']))
            {
                $user->zip_code = $params['zip_code'];
            }

            if(isset($params['duta_code']))
            {
                $user->duta_code = $params['duta_code'];
            }

            //if avatar not null
            if($request->hasFile('avatar')){
                $path       = $this->cdnCourier;
                $pathGet    = $this->getCourier;

                $avatar         = $_FILES['avatar'];
                $filename       = $this->storeFile($avatar, 1, $user, $path, $pathGet);

                $user->avatar   = $filename;
            }

            if($user->save())
            {
                return response()->json([
                    'meta' => ['code' => 200, 'message' => "Berhasil mengubah kurir"]
                ]);
            }
        } catch (\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." : ". $e->getLine()]
            ]);
        }
    }

    public function resetPassword(Request $request)
    {
        //param
        $params             = $request->all();

        $user               = Mitra::find($request->courier_id);
        $user->password     = app('hash')->make('12345678');

        if($user->save())
        {
            return response()->json([
                'meta' => ['code' => 200, 'message' => "Berhasil mereset kata sandi kurir"]
            ]);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "Gagal mereset kata sandi data kurir"]
        ]);
    }

    public function destroy($id)
    {
        $order = Order::where('courier_id', $id)->count();

        if ($order > 0) {

            $user               = Mitra::find($id);
            $user->status       = 0;
            $user->save();

            return response()->json([
                'meta' => ['code' => 200, 'message' => "Berhasil menghapus kurir"]
            ]);
        }

        $delete = Mitra::findOrFail($id)->delete();

        if($delete)
        {
            return response()->json([
                'meta' => ['code' => 200, 'message' => "Berhasil menghapus kurir"]
            ]);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "Gagal menghapus data kurir"]
        ]);
    }

    function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        if($old == 1 && $user->avatar != "") {
            $pathImage = $pathGet."/".$user->avatar;

            if(file_exists($pathImage)){
                unlink($pathImage);
            }
        }

        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["avatar"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }

        // Check file size
        if ($_FILES["avatar"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file)) {

                return $file;

            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }
}
