<?php

namespace App\Http\Controllers\Mitra;

use Validator;
use App\Models\Mitra;
use App\Models\Store;
use App\Models\ApiLog;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends BaseController
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request      = $request;

        $this->cdn          = config('app.cdn');
        $this->cdnMitra     = "../../cdn/avatars/agen/";
        $this->cdnStore     = "../../cdn/avatars/stores/";
        $this->cdnCourier   = "../../cdn/avatars/courier/";
        $this->getMitra     = $this->cdn."/avatars/agen/";
        $this->getStore     = $this->cdn."/avatars/stores/";
        $this->getCourier   = $this->cdn."/avatars/courier/";

        $this->emailUrl     = "http://mailsender.advess.net/";
        $this->apiUrl       = "https://api-dev.primaax.co.id/v2/";
    }

    /**
     * Create a new token.
     *
     * @param  \App\User   $user
     * @return string
     */
    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param  \App\User   $user
     * @return mixed
     */
    public function authenticate(User $user) {
        
        try {
            $this->validate($this->request, [
                'mobile'    => 'required',
                'password'  => 'required'
            ]);
    
            // Find the user by email
            $profile = User::where('username', $this->request->input('mobile'))->first();
    
            if (!$profile) {
                // You wil probably have some sort of helpers or whatever
                // to make sure that you have the same response format for
                // differents kind of responses. But let's return the
                // below respose for now.
                return response()->json([
                    'meta' => ["code" => 400, "message" => 'Mobile phone does not exist.']
                ], 400);
            }
    
            // Verify the password and generate the token
            if (Hash::check($this->request->input('password'), $profile->password)) {
                
                //if agent login
                if($profile->level_id == 3){
                    $user = Mitra::find($profile->mitra_id);
                }

                //if store login
                if($profile->level_id == 4){
                    $user = Store::find($profile->mitra_id);
                }

                $images        = $user->avatar == "" ? $this->cdn."no_image_available.png" : $this->getMitra.$user->avatar;
    
                if($user->is_courier == 1) {
                    unset($user['name_store']);
    
                    $user['name_store'] = isset($user->Mitra) ? $user->Mitra->name_store : "";
                    $images = $user->avatar == "" ? $this->cdn."no_image_available.png" : $this->getCourier.$user->avatar;
                }
    
                $user['image']          = $images;
                $user['no_ktp']         = $user->no_id_card;
                $user['fullname']       = $user->name;
                $user['city']           = isset($user->getCity) ? $user->getCity->city : "";
                $user['city_id']        = $user->city_id;
                $user['province_id']    = isset($user->getCity) ? $user->getCity->province_id : "";
                $user['province']       = isset($user->getCity) ? $user->getCity->Province->province : "";
                $user['token']          = $this->jwt($profile);
                $user['is_store']       = $profile->level_id == 4 ? 1 : 0;
    
                $response             = [
                    'meta' => [ 'code' => 200, 'message' => 'success' ],
                    'data' => $user
                ];

                //send api log
                $apiLog               = new ApiLog;
                $apiLog->url          = $this->apiUrl."mitra/login";
                $apiLog->parameter    = json_encode($this->request->all());
                $apiLog->response     = json_encode($response);
                $apiLog->save();
    
                return response()->json($response, 200);
            } else {

                $response = [
                    'meta' => ["code" => 400, "message" => 'Mobile phone or password is wrong.']
                ];

                $apiLog               = new ApiLog;
                $apiLog->url          = "https://api-dev.primaax.co.id/v2/mitra/login";
                $apiLog->parameter    = json_encode($this->request->all());
                $apiLog->response     = json_encode($response);
                $apiLog->save();

                return response()->json($response, 400);
            }
        } catch (\Exception $e) {
            // Bad Request response
            // return response()->json([
            //     'meta' => ["code" => 400, "message" => 'Mobile phone or password is wrong.']
            // ], 400);
            
            // Bad Request response
            $response             = [
                'meta' => ["code" => 500, "message" => $e->getMessage() ." line: ". $e->getLine()]
            ];

            $apiLog               = new ApiLog;
            $apiLog->url          = "https://api-dev.primaax.co.id/v2/mitra/login";
            $apiLog->parameter    = json_encode($this->request->all());
            $apiLog->response     = json_encode($response);
            $apiLog->save();

            return response()->json($response, 500);
        }

        
    }

    public function refreshToken(Request $request)
    {
        $this->credentials = JWT::decode($request->token, env('JWT_SECRET'), ['HS256']);
        $user              = User::find($this->credentials->sub);

        $response      = array(
            'meta' => array(
                'code' => 404,
                'message' => 'Data user dengan token tersebut tidak tersedia'
            )
        );

        if($user){
            $token         = $this->jwt($user);

            $response      = array(
                'meta' => array(
                    'code' => 200,
                    'message' => 'Berhasil melakukan refresh token'
                ),
                'data' => array(
                    'token' => $token
                )
            );
        }

        return response()->json($response);
    }


    public function profile(Request $request)
    {
        $token       = $request->token;
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

        $user        = User::find($credentials->sub);

        //if agent login
        if($user->level_id == 3){
            $profile = Mitra::find($user->mitra_id);
        }

        //if store login
        if($user->level_id == 4){
            $profile = Store::find($user->mitra_id);
        }

        if($profile)
        {
            $images        = $profile->avatar == "" ? $this->cdn."no_image_available.png" : $this->getMitra.$profile->avatar;

            if($profile->is_courier == 1) {
                unset($profile['name_store']);

                $profile['name_store'] = isset($profile->Mitra) ? $profile->Mitra->name_store : "";
                
                $images = $profile->avatar == "" ? $this->cdn."no_image_available.png" : $this->getCourier.$profile->avatar;
            }

            $profile['image']          = $images;
            $profile['city']           = isset($profile->getCity) ? $profile->getCity->city : "";
            $profile['city_id']        = $profile->city_id;
            $profile['province_id']    = isset($profile->getCity) ? $profile->getCity->province_id : "";
            $profile['province']       = isset($profile->getCity) ? $profile->getCity->Province->province : "";
            $profile['no_ktp']         = $profile->no_id_card;
            $profile['fullname']       = $profile->name;
            $profile['limit']          = $user->limit;

            return response()->json([
                'meta' => ['code' => 200, 'message' => "success"],
                'data' => $profile
            ]);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "Gagal mengambil data profil"]
        ]);
    }

    public function updateProfile(Request $request)
    {
        $token       = $request->token;
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

        //param
        $params      = $request->all();

        $profile     = User::find($credentials->sub);

        //if agent login
        if($profile->level_id == 3){
            $user = Mitra::find($profile->mitra_id);
        }

        //if store login
        if($profile->level_id == 4){
            $user = Store::find($profile->mitra_id);
        }

        $user->name         = $params['fullname'];
        $user->email        = $params['email'];
        $user->mobile       = $params['mobile'];

        if(isset($params['no_ktp']))
        {
            $user->no_id_card = $params['no_ktp'];
        }

        if(isset($params['name_store']))
        {
            $user->name_store = $params['name_store'];
        }

        if(isset($params['city_id']))
        {
            $user->city_id = $params['city_id'];
        }

        if(isset($params['no_ktp']))
        {
            $user->no_id_card = $params['no_ktp'];
        }

        if(isset($params['gender']))
        {
            $user->gender = $params['gender'] == "Male" ? 1 : 2;
        }

        if(isset($params['birth_date']))
        {
            $user->birth_date = date_format(date_create($params['birth_date']),'Y-m-d');
        }

        if(isset($params['address']))
        {
            $user->address = $params['address'];
        }

        if(isset($params['longitude']))
        {
            $user->longitude = $params['longitude'];
        }

        if(isset($params['latitude']))
        {
            $user->latitude = $params['latitude'];
        }

        if(isset($params['zip_code']))
        {
            $user->zip_code = $params['zip_code'];
        }

        if(isset($params['duta_code']))
        {
            $user->duta_code = $params['duta_code'];
        }

        //if password != null
        if($params['password'] != "" || $params['password'] != NULL)
        {
            $user->password = app('hash')->make($params['password']);
        }

        //if avatar not null
        if($request->hasFile('avatar')){

            $path           = $this->cdnMitra;
            $pathGet        = $this->getMitra;

            if($profile->level_id == 4) {
                $path           = $this->cdnStore;
                $pathGet        = $this->getStore;
            }

            if($user->is_courier == 1) {
                $path       = $this->cdnCourier;
                $pathGet    = $this->getCourier;
            }

            $avatar         = $_FILES['avatar'];
            $filename       = $this->storeFile($avatar, 1, $user, $path, $pathGet);

            $user->avatar   = $filename;
        }

        if($user->save())
        {
            //update data mitra and store on table user
            $profile->email     = $params['email'];
            $profile->username  = $params['mobile'];

             //if password != null
            if($params['password'] != "" || $params['password'] != NULL)
            {
                $profile->password  = app('hash')->make($params['password']);
            }

            $profile->save();

            return response()->json([
                'meta' => ['code' => 200, 'message' => "Berhasil mengubah profil"]
            ]);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "Gagal mengubah data profil"]
        ]);
    }

    public function updateStore(Request $request)
    {
        $token       = $request->token;
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

        //param
        $params      = $request->all();

        $profile     = User::find($credentials->sub);

        //if agent login
        if($profile->level_id == 3){
            $user = Mitra::find($profile->mitra_id);
        }

        //if store login
        if($profile->level_id == 4){
            $user = Store::find($profile->mitra_id);
        }

        $user->status_store         = $params['status_store'];

        if($user->save())
        {
            return response()->json([
                'meta' => ['code' => 200, 'message' => "Berhasil mengubah status toko"]
            ]);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "Gagal mengubah status toko"]
        ]);
    }

    public function updateFcm(Request $request)
    {
        $token       = $request->token;
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

        //param
        $params      = $request->all();

        $profile     = User::find($credentials->sub);

        //if agent login
        if($profile->level_id == 3){
            $user = Mitra::find($profile->mitra_id);
        }

        //if store login
        if($profile->level_id == 4){
            $user = Store::find($profile->mitra_id);
        }

        $user->token_fcm    = $params['token_fcm'];

        if($user->save())
        {
            return response()->json([
                'meta' => ['code' => 200, 'message' => "success"]
            ]);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "failed"]
        ]);
    }

    public function updatePassword(Request $request)
    {
        //param
        $params             = $request->all();

        $user               = User::where('email', $params['email'])->first();
        $user->password     = app('hash')->make($params['password']);

        if($user)
        {
            $user->password     = app('hash')->make($params['password']);

            if($user->save())
            {
                return response()->json([ 'message' => "Berhasil mengubah kata sandi" ], 200);
            }
        }

        return response()->json([ 'message' => "failed" ], 404);
    }

    public function forgotPassword(Request $request)
    {
        try{
            $email  = $request->email;

            $user               = User::where('email', $email)->count();

            if($user > 0)
            {
                $url    = "http://api2.rajarumahku.com/customer/newpassword?email=".$email;

                $html   = "Berikut link untuk kata sandi baru : ". $url;

                // Mail::raw($html, function($msg) use ($email) {
                //     $msg->to([$email]);
                //     $msg->from(['developerrizki@gmail.com']);
                //     $msg->subject('Link lupa kata sandi - RRI Customer');
                // });

                $params = 'address='.$email.'&content='.$html.'&subject=Link lupa kata sandi - RRI Customer';

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->emailUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                $result = curl_exec($ch);
                curl_close($ch);

                return response()->json([ 'message' => "Berhasil mengirim permintaan lupa kata sandi" ], 200);
            }

            return response()->json([ 'message' => "Email yang diajukan tidak terdaftar" ], 404);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'line' => $e->getLine()], 404);
        }
    }

    function storeFile($avatar, $old = '', $user = '', $path = '', $pathGet = '')
    {
        if($old == 1 && $user->avatar != "") {
            $pathImage = $pathGet."/".$user->avatar;

            if(file_exists($pathImage)){
                unlink($pathImage);
            }
        }

        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $path;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["avatar"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }

        // Check file size
        if ($_FILES["avatar"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file)) {

                return $file;

            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }
}
