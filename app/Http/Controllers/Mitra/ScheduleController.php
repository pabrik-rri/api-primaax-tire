<?php

namespace App\Http\Controllers\Mitra;

use App\User;
use App\Models\Mitra;
use App\Models\Store;
use App\Models\ManagementStore;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

class ScheduleController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token       = $request->token;
        $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

        $this->profile     = User::find($this->credentials->sub);

        if($this->profile->level_id == 3) {
            $this->user        = Mitra::find($this->profile->mitra_id);
        }

        if($this->profile->level_id == 4) {
            $this->user        = Store::find($this->profile->mitra_id);
        }
    }

    public function index(Request $request)
    {
        $management        = ManagementStore::where('mitra_id',$this->user->id)->get();

        if(sizeof($management) > 0)
        {
            return response()->json([
                'meta' => ['code' => 200, 'message' => "success"],
                'data' => $management
            ], 200);
        } else {

            for ($i=0; $i < 7; $i++) {
                $management = new ManagementStore;
                $management->day = $i+1;
                $management->open_time = "08:00";
                $management->closing_time = "17:00";
                $management->status = 1;
                $management->mitra_id = $this->user->id;
                $management->save();
            }

            $management        = ManagementStore::where('mitra_id',$this->user->id)->get();

            return response()->json([
                'meta' => ['code' => 200, 'message' => "success"],
                'data' => $management
            ], 200);
        }

        return response()->json([
            'meta' => ['code' => 500, 'message' => "Gagal mengambil data faq"]
        ], 500);
    }

    public function create(Request $request, $id =  null)
    {
        try {
            $schedule = ManagementStore::where('mitra_id', $this->user->id)->count();
            $schedule_id = ManagementStore::where('mitra_id', $this->user->id)->pluck('id');

            for ($i=0; $i < sizeof($request->data); $i++) {
                $model = $schedule == 0 ? new ManagementStore : ManagementStore::find($schedule_id[$i]);

                $model->day             = $request->data[$i]["day"];
                $model->open_time       = $request->data[$i]["open_time"];
                $model->closing_time    = $request->data[$i]["closing_time"];
                $model->status          = $request->data[$i]["status"] == "true" ? 1 : 0;
                $model->mitra_id        = $this->user->id;
                $model->save();
            }

            return response()->json([
                'meta' => ['code' => 200, 'message' => "Berhasil melakukan pengaturan jadwal buka / tutup toko"]
            ], 200);

        } catch(\Exception $e) {
            return response()->json([
                'meta' => ['code' => 500, 'message' => $e->getMessage()." - ".$e->getLine()]
            ], 500);
        }
    }
}
