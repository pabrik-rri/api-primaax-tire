<?php

namespace App\Http\Controllers\Mitra;

use App\User;
use App\Models\Mitra;
use App\Models\Store;
use App\Models\Order;
use App\Models\ManagementStore;
use App\Models\OrderDetail;
use App\Models\Product;
use App\Models\ProductMitra;
use App\Models\Banner;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

class HomepageController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token       = $request->token;
        $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

        $this->profile     = User::find($this->credentials->sub);

        if($this->profile->level_id == 3) {
            $this->user        = Mitra::find($this->profile->mitra_id);
            $this->is_store    = 0;
        }

        if($this->profile->level_id == 4) {
            $this->user        = Store::find($this->profile->mitra_id);
            $this->is_store    = 1;
        }

        $this->cdn               = config('app.cdn');
        $this->cdnCustomer       = $this->cdn."/avatars/customer/";
        $this->cdnProducts       = $this->cdn."/products/";
        $this->cdnAds            = $this->cdn."/ads/";
        $this->getCustomer       = $this->cdn."/avatars/customer/";

        $this->objOrder             = new Order;
        $this->modelProductsMitras  = New ProductMitra;
        $this->modelProducts        = New Product;
    }

    public function index(Request $request)
    {
        try {
            $id         = $this->user->id;
            $is_courier = 0;

            if($this->user->is_courier == 1) { 
                $id         = $this->user->mitra_id; 
                $is_courier = 1;
            }

            $role = $this->profile->level_id == 3 ? "mitra" : "store";

            $order        = $this->objOrder->getDataHome($id, $role, $is_courier);

            $orders       = array();

            if ($order) {
               foreach ($order as $key => $value) {
                    switch ($value->status) {
                        case '0':
                            $status = "Pending";
                            break;

                        case '1':
                            $status = "Diterima";
                            break;

                        case '2':
                            $status = "Persiapan Pengiriman";
                            break;

                        case '3':
                            $status = "Dalam Pengiriman";
                            break;

                        case '4':
                            $status = "Batal";
                            break;

                        case '5':
                            $status = "Selesai";
                            break;

                        case '6':
                            $status = "Pindahan";
                            break;

                        default:
                            $status = "";
                            break;
                    }

                    $image = "";

                    if ($value->is_po == 1) {
                        $image = $this->cdnCustomer."/pre-order-profile.png";
                    } else {
                        if(isset($value->Customer) && $value->Customer->avatar != "") {
                            $image = $this->cdnCustomer.$value->Customer->avatar;
                        }
                    }

                    $orders[$key]['orders_id']              = $value->order_id;
                    $orders[$key]['order_code']             = $value->order_code;
                    $orders[$key]['fulname']                = isset($value->Mitra) ? $value->Mitra->name : "";
                    $orders[$key]['address']                = $value->address;
                    $orders[$key]['address_note']           = $value->address_note;
                    $orders[$key]['courier_name']           = isset($value->Courier) ? $value->Courier->name : "";
                    $orders[$key]['customer_name']          = isset($value->Customer) ? $value->Customer->name : "";

                    if($value->is_po == 1) {
                        $orders[$key]['customer_name']      = "Pre Order Barang";
                    }

                    $orders[$key]['customer_phone']         = isset($value->Customer) ? $value->Customer->phone : "";
                    $orders[$key]['customer_address']       = isset($value->Customer) ? $value->Customer->address : "";
                    $orders[$key]['customer_latitude']      = isset($value->Customer) ? $value->Customer->latitude : "";
                    $orders[$key]['customer_longitude']     = isset($value->Customer) ? $value->Customer->longitude : "";
                    $orders[$key]['customer_avatar']        = isset($value->Customer) ? $value->Customer->avatar : "";
                    $orders[$key]['destination_address']    = $value->address;
                    $orders[$key]['destination_latitude']   = $value->latitude;
                    $orders[$key]['destination_longitude']  = $value->longitude;
                    $orders[$key]['payment']                = $value->payment;
                    $orders[$key]['status_payment']         = $value->status_payment;
                    $orders[$key]['reviews']                = $value->review;
                    $orders[$key]['stars']                  = $value->star;
                    $orders[$key]['status']                 = $status;
                    $orders[$key]['discount']               = $value->discount;
                    $orders[$key]['ongkir']                 = $value->ongkir;
                    $orders[$key]['total_price']            = $value->total_price;
                    $orders[$key]['delivery_date']          = $value->delivery_date;
                    $orders[$key]['is_po']                  = $value->is_po;
                    $orders[$key]['order_date']             = date_format(date_create($value->created_at),'Y-m-d H:i:s');
                    $orders[$key]['finish_date']            = date_format(date_create($value->updated_at),'Y-m-d H:i:s');
                    $orders[$key]['image']                  = $image;

                    //detail
                    $orderDetail                    = OrderDetail::where('order_id', $value->order_id)->get();

                    foreach ($orderDetail as $key_detail => $value) {
                        $orders[$key]['detail'][$key_detail]['id']           = $value->id;
                        $orders[$key]['detail'][$key_detail]['name']         = isset($value->Product) ? $value->Product->name : "";
                        $orders[$key]['detail'][$key_detail]['qty']          = $value->qty;
                        $orders[$key]['detail'][$key_detail]['price']        = $value->price;
                        $orders[$key]['detail'][$key_detail]['subtotal']     = ($value->qty * $value->price);
                        $orders[$key]['detail'][$key_detail]['note']         = $value->note;
                    }
               }

            }

            //Incomes
            $income     = $this->objOrder->getIncome($id);

            //Sold
            $sold       = $this->objOrder->getSold($id);

            $management_store = ManagementStore::where('mitra_id', $id)->pluck('day')->toArray();

            if(sizeof($management_store) == 0) {
                for ($i=0; $i < 7; $i++) {
                    $management = new ManagementStore;
                    $management->day = $i+1;
                    $management->open_time = "08:00";
                    $management->closing_time = "17:00";
                    $management->status = 1;
                    $management->mitra_id = $id;
                    $management->save();
                }

                $management_store = ManagementStore::where('mitra_id', $id)->pluck('day')->toArray();
            }

            $timestamp = strtotime(date('Y-m-d')); $day = date('w', $timestamp);

            $today = $day;
            $time = date('H:i');

            if($day == 0) {
                $today = 7;
            }

            if(in_array($today, $management_store)){
                $store = \DB::select(\DB::raw("select * from `management_stores` where `mitra_id` = '". $id ."' and `day` = '". $today ."' AND '". date('H:i') ."' BETWEEN open_time AND closing_time and `status` = 1"));

                $status = sizeof($store) == 0 ? 0 : 1;

                //update
                $mitra = Mitra::find($id);
                $mitra->status_store = $status;
                $mitra->save();
            }

            //get stock
            $productMitra = $this->modelProductsMitras->getDataStock($this->user->id);

            $stock        = array();

            if($productMitra && sizeof($productMitra) > 0) {
                foreach ($productMitra as $key => $value) {
                    $stock[$key]['id']      = $value->id;
                    $stock[$key]['product'] = $value->name;
                    $stock[$key]['qty']     = $value->qty;
                    $stock[$key]['min_qty'] = $value->min_qty;
                    $stock[$key]['unit']    = $value->unit;
                }
            }

            //get new peoduct
            $productNew = $this->modelProducts->getDataNew();

            $products        = array();

            if($productNew && sizeof($productNew) > 0) {
                foreach ($productNew as $key => $value) {
                    $products[$key]['id']                       = $value->id;
                    $products[$key]['name']                     = $value->name;
                    $products[$key]['category']                 = $value->category;
                    $products[$key]['product_category_id']      = $value->product_category_id;
                    $products[$key]['size']                     = $this->modelProducts->getCountCategory($value->product_category_id);
                    $products[$key]['image']                    = $this->cdnProducts.$value->image;
                }
            }

            //banners
            $getBanner = Banner::where('is_customer',0)->get();

            $banners = array();

            if($getBanner){
                foreach ($getBanner as $key => $banner) {

                    $images = $banner->image == "" ? $this->cdn.'/no_image_available.png' : $this->cdnAds.$banner->image;

                    $banners[$key]['title']  = $banner->title;
                    $banners[$key]['image']  = $images;
                }
            }

            //Meta
            $meta       = ["code" => 200, "message" => 'success'];
            $banner_static = $this->cdn.'/no_image_available.png';

            $data       = [
                // "status_store"   => $this->user->status_store,
                "status_store"   => $status,
                "income_today"  => $income == null ? 0 : $income->income_today,
                "already_sold"  => $sold == null ? 0 : $sold->res . " Produk",
                "orders"        => $orders,
                "stocks"        => $stock,
                "new_products"  => $products,
                "banners"       => $banners,
                "banner_static" => $banner_static,
                "is_store"      => $this->is_store  
            ];

            return response()->json([ 'meta' => $meta, 'data' => $data ], 200);
        } catch(\Exception $e) {
            return response()->json([ 'message' => $e->getMessage(), 'line' => $e->getLine() ], 500);
        }

    }
}
