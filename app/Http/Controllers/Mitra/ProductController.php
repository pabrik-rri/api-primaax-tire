<?php

namespace App\Http\Controllers\Mitra;

use App\User;
use App\Models\Product;
use App\Models\ProductCategory;
use App\Models\ProductMitra;
use App\Models\Mitra;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

class ProductController extends BaseController
{
    public function __construct(Request $request)
    {
        $this->token       = $request->token;
        $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

        $this->profile     = User::find($this->credentials->sub);

        if($this->profile->level_id == 3) {
            $this->user        = Mitra::find($this->profile->mitra_id);
        }

        if($this->profile->level_id == 4) {
            $this->user        = Store::find($this->profile->mitra_id);
        }
    }

    /**
	 * [index description]
	 * @return [type] [description]
	 */
	public function index(Request $request)
	{
        try {
            $category       = isset($request->category_id) ? ProductCategory::where('category_id', $request->category_id)->get() : ProductCategory::all();
            $products       = ProductMitra::select([
                                        'product_mitras.id as id',
                                        'products.name as name',
                                        'product_mitras.price as price',
                                        'product_mitras.qty as qty',
                                        'product_mitras.min_qty as min_qty',
                                        'products.product_category_id as product_category_id'
                                    ])
                                    ->join('products','product_mitras.product_id', '=' ,'products.id')
                                    ->join('product_categories','products.product_category_id', '=' ,'product_categories.id')
                                    ->orderBy('products.product_category_id','ASC')
                                    ->where('product_mitras.mitra_id', $this->user->id);

            $products = $products->count();

            $pluck_category  = ProductCategory::pluck('id')->toArray();
    		$data = array();

    		if($products > 0) {

                foreach ($category as $key => $value) {
                    $data[$key]['product_categories'] = $value->name;

                    $product_mitras  = ProductMitra::select([
                                                'product_mitras.id as id',
                                                'products.name as name',
                                                'product_mitras.price as price',
                                                'product_mitras.qty as qty',
                                                'product_mitras.min_qty as min_qty',
                                                'products.product_category_id as product_category_id'
                                            ])
                                            ->join('products','product_mitras.product_id', '=' ,'products.id')
                                            ->join('product_categories','products.product_category_id', '=' ,'product_categories.id')
                                            ->orderBy('products.product_category_id','ASC')
                                            ->where('product_mitras.mitra_id', $this->user->id)
                                            ->where('products.product_category_id', $value->id)
                                            ->get();

                    if(count($product_mitras) > 0) {
                        foreach ($product_mitras as $key2 => $product) {
                            $data[$key]['products'][$key2]['id']       		= $product->id;
                            $data[$key]['products'][$key2]['name']     		= $product->name;
                            $data[$key]['products'][$key2]['price']    		= $product->price;
                            $data[$key]['products'][$key2]['qty']    		= $product->qty;
                            $data[$key]['products'][$key2]['min_qty']    	= $product->min_qty;
                        }
                    } else {
                        $data[$key]['products'] = array();
                    }
                }

        		return response()->json( array( 'message' => 'Berhasil mengambil data produk mitra', 'data' => $data ), 200 );
            } else {
                return response()->json( array( 'message' => 'Data produk mitra tidak tersedia', 'data' => [] ), 200 );
            }
        } catch(\Exception $e) {
            return response()->json( array( 'message' => 'Pengambilan data produk mitra tidak berhasil' ) );
        }
	}

    /**
	 * [categories description]
	 * @return [type] [description]
	 */
    public function categories()
	{
		$categories = Category::all();

		$data = array();
		foreach ($categories as $key => $category) {
			$data[$key]['id']       = $category->id;
            $data[$key]['name']     = $category->name;
		}

		return response()->json( array( 'message' => 'success', 'data' => $data ), 200 );

	}

    public function update(Request $request)
    {
        $params = $request->all();

        if(count($params['data']) > 0) {
            foreach ($params['data'] as $key => $value) {
                $product_mitra = ProductMitra::find($value['id']);
                $product_mitra->min_qty = $value['min_qty'];
                $product_mitra->save();
            }
            return response()->json( array( 'message' => 'Berhasil mengubah minimal stok produk' ), 200 );
        } else {
            return response()->json( array( 'message' => 'Data produk yang diubah tidak tersedia' ), 200 );
        }
    }
}
