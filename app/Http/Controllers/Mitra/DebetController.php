<?php

namespace App\Http\Controllers\Mitra;

use DB;
use App\User;
use App\Models\Mitra;
use App\Models\Store;
use App\Models\Receivables;
use App\Models\Receipt;
use App\Models\Debts;
use App\Models\Order;
use App\Models\DebtDetails;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class DebetController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token       = $request->token;
        $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

        $this->profile     = User::find($this->credentials->sub);

        if($this->profile->level_id == 3) {
            $this->user        = Mitra::find($this->profile->mitra_id);
        }

        if($this->profile->level_id == 4) {
            $this->user        = Store::find($this->profile->mitra_id);
        }

        $this->cdn        = config('app.cdn');
        $this->modelOrder = new Order;
    }

    public function index()
    {
        $debts = DebtDetails::join('debts','debt_details.debt_id', '=' ,'debts.id')
                    ->join('receivables','debt_details.receivable_id', '=' ,'receivables.id')
                    ->where('debts.customer_name', $this->user->name)
                    ->whereNotNull('receivables.order_id')
                    ->orderBy('debt_date', 'desc')
                    ->get();

        // return $debts; exit;
        
        $data       = array();
        $meta       = array();
        $response   = array();

        if($debts){
            $limit_debt = $this->user->limit - $this->modelOrder->getLimitUse($this->user->id, $this->profile->level_id);

            if(count($debts) > 0){
                foreach ($debts as $key => $value) {
                    $data[$key]['code']     = isset($value->Receivables->Order) ? $value->Receivables->Order->order_code : "-";
                    $data[$key]['date']     = date('Y-m-d H:i:s', strtotime($value->debt_date));
                    $data[$key]['price']    = $value->value_of_debt;
                    $data[$key]['status']   = $value->status == 0 ? "Konfirmasi" : "Selesai";
                }

                $meta = array(
                    'code' => 200,
                    'message' => "success"
                );

                $response = array('meta' => $meta, 'data' => $data, 'limit_debt' => $limit_debt);

            }else{

                $meta = array(
                    'code' => 200,
                    'message' => "success"
                );

                $response = array('meta' => $meta, 'data' => $data, 'limit_debt' => $limit_debt);
            }

        }else {

            $meta = array(
                'code' => 500,
                'message' => "error"
            );

            $response = array('meta' => $meta, 'data' => $data);
        }

        return response()->json($response);
    }

    public function create()
    {
        $receivables = Receivables::where('customer_name', $this->user->name)
                            ->where('balance_of_receivable', '>', 0)
                            ->orderBy('id', 'asc')->get();

        $data       = array();
        $meta       = array();
        $response   = array();
        $total      = 0;

        if($receivables){
            
            if(count($receivables) > 0){
                foreach ($receivables as $key => $value) {
                    $data[$key]['code']         = isset($value->Order) ? $value->Order->order_code : "-";
                    $data[$key]['date']         = date('Y-m-d H:i:s', strtotime($value->invoice_date));
                    $data[$key]['price']        = $value->balance_of_receivable;

                    $total                      = $total + $value->balance_of_receivable;
                }

                $meta = array(
                    'code' => 200,
                    'message' => "success"
                );

                $response = array('meta' => $meta, 'data' => $data, 'total_debt' => $total);

            }else{

                $meta = array(
                    'code' => 404,
                    'message' => "data not available"
                );

                $response = array('meta' => $meta, 'data' => $data);
            }

        }else {

            $meta = array(
                'code' => 500,
                'message' => "error"
            );

            $response = array('meta' => $meta, 'data' => $data);
        }
        return response()->json($response);
    }

    public function store(Request $request)
    {
        $data       = array();
        $meta       = array();
        $response   = array();

        try {
            $params = $request->all();

            DB::transaction(function() use ($params) {

                $last_returns = Debts::where('code', 'like', '%/'.date('n').'/'.date('Y'))->orderBy('code', 'desc')->first();
                if (!$last_returns) {
                    $code = '00001/PH/' . date('n') . '/' . date('Y');
                } else {
                    $ex = explode('/', $last_returns->code);
                    $increment = $ex[0];
                    if (date('n') == $ex[2] && date('Y') == $ex[3])
                        $code = str_pad($increment + 1, 5, '0', STR_PAD_LEFT) . '/PH/' . date('n') . '/' . date('Y');
                    else
                        $code = '00001/PH/' . date('n') . '/' . date('Y');
                }

                $payment = $params['payment'];

                DB::table('debts')->insert([
                    'code' => $code,
                    'debt_date' => date('Y-m-d H:i:s'),
                    'supplier_id' => $this->profile->level_id == 3 ? 2 : $this->user->mitra_id,
                    'customer_name' => $this->user->name,
                    'value_of_debt' => $payment,
                    'status' => 0,
                    'payment_method' => 0,
                    'is_factory' => 1,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ]);

                $debt_id = DB::getPdo()->lastInsertId();

                $receivables = Receivables::where('customer_name', $this->user->name)
                                ->where('balance_of_receivable', '>', 0)
                                ->orderBy('id', 'asc')->get();

                $insert = [];
                foreach ($receivables as $value) {
                    if ($payment <= 0)
                        break;
                    if ($value->balance_of_receivable - $payment <= 0) {
                        $saldo_hutang = $value->balance_of_receivable;
                        $payment -= $saldo_hutang;
                        $saldo_piutang = 0;
                    } else {
                        $saldo_hutang = $payment;
                        $payment -= $saldo_hutang;
                        $saldo_piutang = $value->balance_of_receivable - $saldo_hutang;
                    }

                    // Update Saldo Piutang dengan saldo piutang = 0
                    Receivables::where('id', $value->id)
                        ->update(['balance_of_receivable' => $saldo_piutang]);

                    $insert[] = [
                        'receivable_id' => $value->id,
                        'debt_id' => $debt_id,
                        'balance' => $saldo_hutang,
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s'),
                    ];
                }

                DB::table('debt_details')->insert($insert);
            });

            $meta = array(
                'code' => 200,
                'message' => "success"
            );

            //get payment info
            $data['bank_logo']      = $this->cdn."banks/logo-bca.png";
            $data['bank_name']      = "Pabrik Primaax";
            $data['bank_number']    = "658 232 0598";
            $data['total_payment']  = $params['payment'];

            $response = array('meta' => $meta, 'data' => $data);

        } catch (\Exception $e) {
            $meta = array(
                'code' => 200,
                'message' => $e->getMessage()." line: ". $e->getLine()
            );

            $response = array('meta' => $meta, 'data' => $data);
        }

        return response()->json($response);
    }
}
