<?php

namespace App\Http\Controllers\Customer;

use App\Models\Voucher;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

class VoucherController extends BaseController
{
    public function index(Request $request)
    {
        $voucher      = Voucher::all();

        if($voucher)
        {
            return response()->json([
                'message' => "success",
                'data' => $voucher
            ], 200);
        }

        return response()->json([
            'message' => "Gagal mengambil data voucher"
        ], 500);
    }

    public function reedem(Request $request)
    {
        $voucher = Voucher::where('code', $request->voucher)->first();

        $today   = date('Y-m-d');

        if ($voucher) {

					$limit_voucher = 0;
					
					if($request->session()->has('email_customer')){
							$email_customer = $request->session()->get('email_customer');
							$customer = UserMobile::where('email', $email_customer)->first();
							$limit_voucher = Order::where('user_id', $customer->id)->where('voucher_id', $voucher->id)->count();
					}

					if($limit_voucher != 0)
					{
						$result = array(
							'message' => "Anda sudah melebihi batas penggunaan voucher"
						);

						return response()->json( $result, 404 );
					} else {
						$expired = Voucher::where('code', $voucher->code)
                        ->whereRaw("'". $today ."' BETWEEN DATE(start) AND DATE(finish)")->first();

						if($expired)
						{
							$result = array(
								'message' => "success",
								'data'    => array(
									'id'	   		=> $voucher->id,
									'code'	   		=> $voucher->code,
									'description'	=> $voucher->description,
									'discount' 		=> $voucher->value
								)
							);

							return response()->json( $result, 200 );
						} else {
							$result = array(
								'message' => "Voucher ". $request->session()->get('email_customer') ." ini berlaku dari tanggal ". date_format(date_create($voucher->start),'d F Y') ." s/d ". date_format(date_create($voucher->finish),'d F Y'),
							);

							return response()->json( $result, 404 );
						}
					}

          
        } else {
          return response()->json( array('message' => 'Voucher tidak tersedia'), 404 );
        }
    }
}
