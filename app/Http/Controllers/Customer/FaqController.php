<?php

namespace App\Http\Controllers\Customer;

use App\Models\Customer;
use App\Models\Faq;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

class FaqController extends BaseController
{
    public function index(Request $request)
    {
        $faq        = Faq::where('role','user')->get();

        if($faq)
        {
            return response()->json([
                'message' => "success",
                'data' => $faq
            ], 200);
        }

        return response()->json([
            'message' => "Gagal mengambil data faq"
        ], 500);
    }

    public function detail(Request $request, $id)
    {
        $faq        = Faq::find($id);

        if($faq)
        {
            return response()->json([
                'message' => "success",
                'data' => $faq
            ],200);
        }

        return response()->json([
            'message' => "Gagal mengambil data detail faq"
        ], 500);
    }
}
