<?php

namespace App\Http\Controllers\Customer;

use Validator;
use App\Models\Customer;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Crypt;
use Laravel\Lumen\Routing\Controller as BaseController;

class AuthController extends BaseController
{
    /**
     * The request instance.
     *
     * @var \Illuminate\Http\Request
     */
    private $request;

    /**
     * Create a new controller instance.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     */
    public function __construct(Request $request) {
        $this->request = $request;

        $this->cdn          = config('app.cdn');
        $this->cdnAva       = "../../cdn/avatars/customer/";
        $this->getAva       = $this->cdn."/avatars/customer/";

        $this->emailUrl     = "http://mailsender.advess.net/";
    }

    /**
     * Create a new token.
     *
     * @param  \App\User   $user
     * @return string
     */
    protected function jwt(Customer $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

    /**
     * Authenticate a user and return the token if the provided credentials are correct.
     *
     * @param  \App\User   $user
     * @return mixed
     */
    public function authenticate(Customer $user) {
        $this->validate($this->request, [
            'mobile'    => 'required',
            'password'  => 'required'
        ]);

        // Find the user by email
        $user = Customer::where('mobile', $this->request->input('mobile'))->first();

        if (!$user) {
            // You wil probably have some sort of helpers or whatever
            // to make sure that you have the same response format for
            // differents kind of responses. But let's return the
            // below respose for now.
            return response()->json([
                "message" => 'No Handphone tidak tersedia.'
            ], 400);
        }

        // Verify the password and generate the token
        if (Hash::check($this->request->input('password'), $user->password)) {
            $images         = $user->avatar == "" ? $this->cdn."no_image_available.png" : $this->getAva.$user->avatar;

            $user['image']          = $images;
            $user['no_ktp']         = $user->no_id_card;
            $user['fullname']       = $user->name;
            $user['provinsi']       = isset($value->getCity) ? $value->getCity->Province->province : "";
            $user['provinsi_id']    = isset($value->getCity) ? $value->getCity->province_id : "";
            $user['city']           = isset($value->getCity) ? $value->getCity->city : "";
            $user['token']          = $this->jwt($user);

            return response()->json([
                'message' => 'success',
                'data' => $user
            ], 200);
        }

        // Bad Request response
        return response()->json([
            "message" => 'No Handphone atau kata sandi salah.'
        ], 400);
    }

    public function refreshToken(Request $request)
    {
        $this->credentials = JWT::decode($request->token, env('JWT_SECRET'), ['HS256']);
        $user              = Customer::find($this->credentials->sub);

        $response      = array(
            'meta' => array(
                'code' => 404,
                'message' => 'Data user dengan token tersebut tidak tersedia'
            )
        );

        if($user){
            $token         = $this->jwt($user);

            $response      = array(
                'meta' => array(
                    'code' => 200,
                    'message' => 'Berhasil melakukan refresh token'
                ),
                'data' => array(
                    'token' => $token
                )
            );
        }

        return response()->json($response);
    }


    public function profile(Request $request)
    {
        $token       = $request->token;
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

        $user        = Customer::find($credentials->sub);

        if($user)
        {
            $images        = $user->avatar == "" ? $this->cdn."no_image_available.png" : $this->getAva.$user->avatar;

            $user['image']         = $images;
            $user['fullname']      = $user->name;
            $user['no_ktp']        = $user->no_id_card;
            $user['provinsi']      = isset($value->getCity) ? $value->getCity->Province->province : "";
            $user['provinsi_id']   = isset($value->getCity) ? $value->getCity->province_id : "";
            $user['city']          = isset($value->getCity) ? $value->getCity->city : "";

            return response()->json([
                'message' => "success",
                'data' => $user
            ], 200);
        }

        return response()->json([
            'message' => "Gagal mengambil data profil"
        ],404);
    }

    public function store(Request $request)
    {
        try{
            //param
            $params      = $request->all();
            
            $available   = Customer::where('mobile', $params['mobile'])->count();

            if($available == 0)
            {
                $user               = new Customer;
                $user->name         = $params['fullname'];
                $user->email        = $params['email'];
                $user->no_id_card   = isset($params['no_ktp']) ? $params['no_ktp'] : "";
                $user->mobile       = $params['mobile'];
                $user->gender       = isset($params['gender']) ? $params['gender'] : "";
                $user->birth_date   = isset($params['birth_date']) ? $params['birth_date'] : "";
                $user->address      = isset($params['address']) ? $params['address'] : "";
                $user->latitude     = isset($params['latitude']) ? $params['latitude'] : "";
                $user->longitude    = isset($params['longitude']) ? $params['longitude'] : "";
                $user->city_id      = isset($params['city_id']) ? $params['city_id'] : "";
                $user->zip_code     = isset($params['zip_code']) ? $params['zip_code'] : "";
                $user->referral_code     = isset($params['referral_code']) ? $params['referral_code'] : "";
                $user->password     = app('hash')->make($params['password']);
                $user->status       = 1;


                //if avatar not null
                if($request->hasFile('avatar')){
                    $filename = $this->storeFile($_FILES['avatar'], 0, '');
                    $user->avatar = $filename;
                }

                if($user->save())
                {
                    return response()->json([ 'message' => "Berhasil mendaftar" ], 200);
                }
            } else {
                return response()->json([ 'message' => "No Handphone sudah terdaftar, silahkah menggunakan no handphone lain" ], 404);
            }

        } catch(\Exception $e){
            return response()->json(['message' => $e->getMessage(), 'line' => $e->getLine()], 404);
        }


    }

    public function updateProfile(Request $request)
    {
        $token       = $request->token;
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

        try{
            //param
            $params      = $request->all();

            $user                   = Customer::find($credentials->sub);
            $user->name             = $params['fullname'];
            $user->email            = $params['email'];
            $user->mobile           = $params['mobile'];

            if(isset($params['city_id']))
            {
                $user->city_id = $params['city_id'];
            }

            if(isset($params['no_ktp']))
            {
                $user->no_id_card = $params['no_ktp'];
            }

            if(isset($params['gender']))
            {
                $user->gender = $params['gender'];
            }

            if(isset($params['birth_date']))
            {
                $user->birth_date = date_format(date_create($params['birth_date']),'Y-m-d');
            }

            if(isset($params['address']))
            {
                $user->address = $params['address'];
            }

            if(isset($params['longitude']))
            {
                $user->longitude = $params['longitude'];
            }

            if(isset($params['latitude']))
            {
                $user->latitude = $params['latitude'];
            }

            if(isset($params['zip_code']))
            {
                $user->zip_code = $params['zip_code'];
            }

            if(isset($params['referral_code']))
            {
                $user->referral_code = $params['referral_code'];
            }

            //if password != null
            if(isset($params['duta_code']))
            {
                $user->password = app('hash')->make($params['password']);
            }

            //if avatar not null
            if($request->hasFile('avatar')){
                $filename = $this->storeFile($_FILES['avatar'], 1, $user);
                $user->avatar = $filename;
            }

            if($user->save())
            {
                return response()->json(['message' => "Berhasil mengubah profil"], 200);
            }
        } catch(\Exception $e){
            return response()->json(['message' => $e->getMessage(), 'line' => $e->getLine()], 404);
        }

        // return response()->json(['message' => "Gagal mengubah data profil"], 404);
    }

    public function updateFcm(Request $request)
    {
        $token       = $request->token;
        $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);

        //param
        $params      = $request->all();

        $user               = Customer::find($credentials->sub);
        $user->token_fcm    = $params['token_fcm'];

        if($user->save())
        {
            return response()->json([ 'message' => "success" ], 200);
        }

        return response()->json([ 'message' => "failed" ], 404);
    }

    public function updatePassword(Request $request)
    {
        //param
        $params             = $request->all();

        $user               = Customer::where('email', $params['email'])->first();

        if($user)
        {
            $user->password     = app('hash')->make($params['password']);

            if($user->save())
            {
                return response()->json([ 'message' => "Berhasil mengubah kata sandi" ], 200);
            }
        }

        return response()->json([ 'message' => "failed" ], 404);
    }

    public function forgotPassword(Request $request)
    {
        try{
            $email  = $request->email;

            $user               = Customer::where('email', $email)->count();

            if($user > 0)
            {
                $url    = "http://api2.rajarumahku.com/customer/newpassword?email=".$email;

                $html   = "Berikut link untuk kata sandi baru : ". $url;

                $params = 'address='.$email.'&content='.$html.'&subject=Link lupa kata sandi - RRI Customer';

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $this->emailUrl);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
                $result = curl_exec($ch);
                curl_close($ch);

                return response()->json([ 'message' => "Berhasil mengirim permintaan lupa kata sandi" ], 200);
            }

            return response()->json([ 'message' => "Email yang diajukan tidak terdaftar" ], 404);

        } catch(\Exception $e) {
            return response()->json(['message' => $e->getMessage(), 'line' => $e->getLine()], 404);
        }
    }

    function storeFile($avatar, $old = '', $user = '')
    {
        if($old == 1 && $user->avatar != "") {
            $pathImage = $this->getAva."/".$user->avatar;

            if(file_exists($pathImage)){
                unlink($pathImage);
            }
        }

        $userfile       = explode(".", strtolower($avatar['name']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $this->cdnAva;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["avatar"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }

        // Check file size
        if ($_FILES["avatar"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["avatar"]["tmp_name"], $target_file)) {

                return $file;

            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }
}
