<?php

namespace App\Http\Controllers\Customer;

use App\Models\Customer;
use App\Models\Product;
use App\Models\Category;
use App\Models\Banner;
use App\Models\Slider;
use App\Models\Order;
use App\Models\OrderDetail;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

class HomepageController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token       = $request->token == NULL ? "" : $request->token;

        if($this->token != NULL){
            $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

            $this->user        = Customer::find($this->credentials->sub);
        }

        $this->objOrder    = new Order;

        $this->cdn         = config('app.cdn');
        $this->cdnCategory = $this->cdn."/categories/";
        $this->cdnProduct  = $this->cdn."/products/";
        $this->cdnAvatar   = $this->cdn."/avatar/";
        $this->cdnAds      = $this->cdn."/ads/";
        $this->cdnMitra    = $this->cdn."/avatars/mitra/";
        $this->cdnSlider   = $this->cdn."/sliders/";
    }

    /**
   	 * [home_customer description]
   	 * @return [type] [description]
   	 */
    public function index()
    {
        $getCategory = Category::all();
        $categories  = array();

        if($getCategory){
            foreach ($getCategory as $key => $category) {

                $images = $category->banner == "" ? $this->cdn.'/no_image_available.png' : $this->cdnCategory.$category->banner;

                $categories[$key]['id']     = $category->id;
                $categories[$key]['name']   = $category->name;
                $categories[$key]['image']  = $images;
            }
        }

        $getBanner = Banner::where('is_customer',1)->get();

        $banners = array();

        if($getBanner){
            foreach ($getBanner as $key => $banner) {

                $images = $banner->image == "" ? $this->cdn.'/no_image_available.png' : $this->cdnAds.$banner->image;

                $banners[$key]['title']  = $banner->title;
                $banners[$key]['image']  = $images;
            }
        }

        //best_product
        $products_id 		= \App\Models\OrderDetail::select('product_id',
                                \DB::raw('count(product_id) as best'))
                                ->orderBy(\DB::raw('best'))
                                ->groupBy('product_id')
                                ->pluck('product_id');

        $best 				= Product::whereIn('id',$products_id)->limit(10)->get();
        $best_products  	= array();

        if($best)
        {
            foreach ($best as $key => $value)
            {
                $p_category = \App\Models\ProductCategory::find($value->product_category_id);

                if($p_category)
                {
                    $category['id'] 	= $p_category->id;
                    $category['name'] 	= $p_category->name;
                    $category['image'] 	= $p_category->images == "" ? $this->cdn.'/no_image_available.png' : $this->cdnCategory.$p_category->images;
                }

                $best_products[$key]['id'] 				    = $value->id;
                $best_products[$key]['code'] 			    = $value->code;
                $best_products[$key]['name'] 			    = $value->name;
                $best_products[$key]['category'] 		    = $value->category;
                $best_products[$key]['price'] 			    = $value->price_market;
                $best_products[$key]['qty'] 			    = $value->qty;
                $best_products[$key]['min_qty'] 		    = $value->min_qty;
                $best_products[$key]['description'] 	    = $value->description;
                $best_products[$key]['image'] 			    = $this->cdnProduct.$value->image;
                $best_products[$key]['created_at'] 		    = date_format($value->created_at,'Y-m-d H:i:s');
                $best_products[$key]['updated_at'] 		    = date_format($value->updated_at,'Y-m-d H:i:s');
                $best_products[$key]['product_category']	= $category;
            }
        }

        $orders       = array();

        if($this->token != NULL)
        {
            $id           = $this->user->id;
            $order        = $this->objOrder->getDataHome($id, 'customer', 0);

            if ($order) {
                foreach ($order as $key => $value) {
                    $image = "";

                    if(isset($value->Mitra) && $value->Mitra->avatar != "") {
                        $image = $this->cdnMitra.$value->Mitra->avatar;
                    }

                    $orders[$key]['orders_id']              = $value->order_id;
                    $orders[$key]['order_code']             = $value->order_code;
                    $orders[$key]['fulname']                = isset($value->Mitra) ? $value->Mitra->name : "";
                    $orders[$key]['name_store']             = isset($value->Mitra) ? $value->Mitra->name_store : "";
                    $orders[$key]['mitra_address']          = isset($value->Mitra) ? $value->Mitra->address : "";
                    $orders[$key]['address']                = $value->address;
                    $orders[$key]['address_note']           = $value->address_note;
                    $orders[$key]['courier_name']           = isset($value->Courier) ? $value->Courier->name : "";
                    $orders[$key]['customer_name']          = isset($value->Customer) ? $value->Customer->name : "";
                    $orders[$key]['customer_phone']         = isset($value->Customer) ? $value->Customer->phone : "";
                    $orders[$key]['customer_address']       = isset($value->Customer) ? $value->Customer->address : "";
                    $orders[$key]['customer_latitude']      = isset($value->Customer) ? $value->Customer->latitude : "";
                    $orders[$key]['customer_longitude']     = isset($value->Customer) ? $value->Customer->longitude : "";
                    $orders[$key]['customer_avatar']        = isset($value->Customer) ? $value->Customer->avatar : "";
                    $orders[$key]['destination_address']    = $value->address;
                    $orders[$key]['destination_latitude']   = $value->latitude;
                    $orders[$key]['destination_longitude']  = $value->longitude;
                    $orders[$key]['payment']                = $value->payment;
                    $orders[$key]['status_payment']         = $value->status_payment;
                    $orders[$key]['reviews']                = $value->review;
                    $orders[$key]['stars']                  = $value->star;
                    $orders[$key]['status']                 = "Selesai";
                    $orders[$key]['discount']               = $value->discount;
                    $orders[$key]['ongkir']                 = $value->ongkir;
                    $orders[$key]['total_price']            = $value->total_price;
                    $orders[$key]['delivery_date']          = $value->delivery_date;
                    $orders[$key]['order_date']             = date_format(date_create($value->order_date),'Y-m-d H:i:s');
                    $orders[$key]['finish_date']            = date_format(date_create($value->finish_date),'Y-m-d H:i:s');
                    $orders[$key]['image']                  = $image;

                    //detail
                    $orderDetail                    = OrderDetail::where('order_id', $value->order_id)->get();

                    foreach ($orderDetail as $key_detail => $value) {
                        $orders[$key]['detail'][$key_detail]['id']           = $value->product_id;
                        $orders[$key]['detail'][$key_detail]['name']         = isset($value->Product) ? $value->Product->name : "";
                        $orders[$key]['detail'][$key_detail]['qty']          = $value->qty;
                        $orders[$key]['detail'][$key_detail]['price']        = $value->price;
                        $orders[$key]['detail'][$key_detail]['subtotal']     = ($value->qty * $value->price);
                        $orders[$key]['detail'][$key_detail]['note']         = $value->note;
                    }
                }
            }
        }

        $data = array(
            'banners' => $banners,
            'categories' => $categories,
            'new_products' => [],
            'best_sellers' => $best_products,
            'last_order' => $orders
        );

        return response()->json( array( 'message' => 'success', 'data' => $data ), 200 );
    }
}
