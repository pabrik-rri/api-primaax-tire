<?php

namespace App\Http\Controllers\Customer;

use App\Models\Customer;
use App\Models\AddressBook;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Laravel\Lumen\Routing\Controller as BaseController;

class AddressController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token       = $request->token;
        $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

        $this->user        = Customer::find($this->credentials->sub);
    }

    public function index(Request $request)
    {
        $books        = AddressBook::where('user_id', $this->user->id)->get();

        if($books)
        {
            return response()->json([ 'message' => "success", 'data' => $books], 200);
        }

        return response()->json(['message' => "Gagal mengambil data buku alamat"], 404);
    }

    public function store(Request $request)
    {
        $params = $request->all();

        $books                  = New AddressBook;
        $books->address         = $params['address'];
        $books->address_note    = $params['address_note']; 
        $books->longitude       = $params['longitude'];
        $books->latitude        = $params['latitude'];
        $books->user_id         = $this->user->id;

        if($books->save()){
            return response()->json([ 'message' => "Berhasil menambahkan alamat baru"], 200);
        }

        return response()->json(['message' => "Gagal menambahkan alamat baru"], 404);
    }

    public function destroy($id)
    {
        $delete = AddressBook::findOrFail($id)->delete();

        if($delete)
        {
            return response()->json([
                'message' => "Berhasil menghapus alamat"
            ], 200);
        }

        return response()->json([
            'message' => "Gagal menghapus data alamat"
        ],500);
    }
}
