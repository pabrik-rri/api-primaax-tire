<?php

namespace App\Http\Controllers\Customer;

use App\Models\Mitra;
use App\Models\Customer;
use App\Models\Voucher;
use App\Models\Order;
use App\Models\Store;
use App\Models\Product;
use App\Models\Payment;
use App\Models\OrderDetail;
use App\Models\Radius;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class OrderController extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token       = $request->token;
        $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

        $this->user        = Customer::find($this->credentials->sub);

        $this->cdn         = config('app.cdn');
        $this->cdnMitra    = $this->cdn."/avatars/mitra/";

        $this->objOrder    = new Order;
        $this->notif       = app('App\Http\Controllers\NotificationController');

        $this->modulStores = new Store;
    }

    public function index()
    {
        $id           = $this->user->id;

        $order        = $this->objOrder->getDataOrder($id,'customer', 0);

        $data         = array();

        if($order)
        {

            $data = array();

            if(sizeof($order) > 0)
            {
                foreach ($order as $key => $value)
                {
                    switch ($value->status) {
                        case '0':
                            $status = "Pending";
                            break;

                        case '1':
                            $status = "Diterima";
                            break;

                        case '2':
                            $status = "Persiapan Pengiriman";
                            break;

                        case '3':
                            $status = "Dalam Pengiriman";
                            break;

                        case '4':
                            $status = "Batal";
                            break;

                        case '5':
                            $status = "Selesai";
                            break;

                        case '6':
                            $status = "Pindahan";
                            break;

                        default:
                            $status = "";
                            break;
                    }

                    $image = "";

                    if(isset($value->Mitra) && $value->Mitra->avatar != "") {
                        $image = $this->cdnMitra.$value->Mitra->avatar;
                    }

                    $data[$key]['orders_id']              = $value->order_id;
                    $data[$key]['order_code']             = $value->order_code;
                    $data[$key]['fulname']                = isset($value->Mitra) ? $value->Mitra->name : "";
                    $data[$key]['name_store']             = isset($value->Mitra) ? $value->Mitra->name_store : "";
                    $data[$key]['address']                = $value->address;
                    $data[$key]['mitra_address']          = isset($value->Mitra) ? $value->Mitra->address : "";
                    $data[$key]['mitra_latitude']         = isset($value->Mitra) ? $value->Mitra->latitude : "";
                    $data[$key]['mitra_longitude']        = isset($value->Mitra) ? $value->Mitra->longitude : "";
                    $data[$key]['address']                = $value->address;
                    $data[$key]['latitude']               = $value->latitude;
                    $data[$key]['longitude']              = $value->longitude;
                    $data[$key]['address_note']           = $value->address_note;
                    $data[$key]['courier_id']             = $value->courier_id;
                    $data[$key]['courier_name']           = isset($value->Courier) ? $value->Courier->name : "";
                    $data[$key]['customer_name']          = isset($value->Customer) ? $value->Customer->name : "";
                    $data[$key]['customer_phone']         = isset($value->Customer) ? $value->Customer->mobile : "";
                    $data[$key]['customer_address']       = $value->address;
                    $data[$key]['customer_latitude']      = $value->latitude;
                    $data[$key]['customer_longitude']     = $value->longitude;
                    $data[$key]['destination_address']    = $value->address;
                    $data[$key]['destination_latitude']   = $value->latitude;
                    $data[$key]['destination_longitude']  = $value->longitude;
                    $data[$key]['customer_avatar']        = "";
                    $data[$key]['payment']                = $value->payment;
                    $data[$key]['status_payment']         = $value->status_payment;
                    $data[$key]['reviews']                = $value->review;
                    $data[$key]['stars']                  = $value->star;
                    $data[$key]['status']                 = $status;
                    $data[$key]['discount']               = $value->discount;
                    $data[$key]['ongkir']                 = $value->ongkir;
                    $data[$key]['total_price']            = $value->total_price;
                    $data[$key]['delivery_date']          = $value->delivery_date;
                    $data[$key]['order_date']             = date_format(date_create($value->order_date),'Y-m-d H:i:s');
                    $data[$key]['finish_date']            = date_format(date_create($value->finish_date),'Y-m-d H:i:s');
                    $data[$key]['image']                  = $image;
                }
            }

            return response()->json([
                'message' => "success",
                'data' => $data
            ], 200);
        }

        return response()->json([
            'message' => "Gagal mengambil data order"
        ], 404);
    }

    public function history(Request $request)
    {
        $id        = $this->user->id;

        $order        = $this->objOrder->getDataHistory($id,'customer',0);

        $data         = array();

        if($order)
        {
            $data = array();

            if(sizeof($order) > 0)
            {
                foreach ($order as $key => $value) {

                    switch ($value->status) {
                        case '0':
                            $status = "Pending";
                            break;

                        case '1':
                            $status = "Diterima";
                            break;

                        case '2':
                            $status = "Persiapan Pengiriman";
                            break;

                        case '3':
                            $status = "Dalam Pengiriman";
                            break;

                        case '4':
                            $status = "Batal";
                            break;

                        case '5':
                            $status = "Selesai";
                            break;

                        case '6':
                            $status = "Pindahan";
                            break;

                        default:
                            $status = "";
                            break;
                    }

                    $image = "";

                    if(isset($value->Mitra) && $value->Mitra->avatar != "") {
                        $image = $this->cdnMitra.$value->Mitra->avatar;
                    }

                    $data[$key]['orders_id']              = $value->order_id;
                    $data[$key]['order_code']             = $value->order_code;
                    $data[$key]['fulname']                = isset($value->Mitra) ? $value->Mitra->name : "";
                    $data[$key]['name_store']             = isset($value->Mitra) ? $value->Mitra->name_store : "";
                    $data[$key]['address']                = $value->address;
                    $data[$key]['mitra_address']          = isset($value->Mitra) ? $value->Mitra->address : "";
                    $data[$key]['mitra_latitude']         = isset($value->Mitra) ? $value->Mitra->latitude : "";
                    $data[$key]['mitra_longitude']        = isset($value->Mitra) ? $value->Mitra->longitude : "";
                    $data[$key]['address']                = $value->address;
                    $data[$key]['latitude']               = $value->latitude;
                    $data[$key]['longitude']              = $value->longitude;
                    $data[$key]['address_note']           = $value->address_note;
                    $data[$key]['courier_id']             = $value->courier_id;
                    $data[$key]['courier_name']           = isset($value->Courier) ? $value->Courier->name : "";
                    $data[$key]['customer_name']          = isset($value->Customer) ? $value->Customer->name : "";
                    $data[$key]['customer_phone']         = isset($value->Customer) ? $value->Customer->mobile : "";
                    $data[$key]['customer_address']       = $value->address;
                    $data[$key]['customer_latitude']      = $value->latitude;
                    $data[$key]['customer_longitude']     = $value->longitude;
                    $data[$key]['destination_address']    = $value->address;
                    $data[$key]['destination_latitude']   = $value->latitude;
                    $data[$key]['destination_longitude']  = $value->longitude;
                    $data[$key]['customer_avatar']        = "";
                    $data[$key]['payment']                = $value->payment;
                    $data[$key]['status_payment']         = $value->status_payment;
                    $data[$key]['reviews']                = $value->review;
                    $data[$key]['stars']                  = $value->star;
                    $data[$key]['status']                 = $status;
                    $data[$key]['discount']               = $value->discount;
                    $data[$key]['ongkir']                 = $value->ongkir;
                    $data[$key]['total_price']            = $value->total_price;
                    $data[$key]['delivery_date']          = $value->delivery_date;
                    $data[$key]['order_date']             = date_format(date_create($value->order_date),'Y-m-d H:i:s');
                    $data[$key]['finish_date']            = date_format(date_create($value->finish_date),'Y-m-d H:i:s');
                    $data[$key]['image']                  = $image;

                    //detail
                    $orderDetail                    = OrderDetail::where('order_id', $value->order_id)->get();

                    foreach ($orderDetail as $key_detail => $value) {
                        $data[$key]['detail'][$key_detail]['id']           = $value->product_id;
                        $data[$key]['detail'][$key_detail]['name']         = isset($value->Product) ? $value->Product->name : "";
                        $data[$key]['detail'][$key_detail]['qty']          = $value->qty;
                        $data[$key]['detail'][$key_detail]['price']        = $value->price;
                        $data[$key]['detail'][$key_detail]['subtotal']     = ($value->qty * $value->price);
                        $data[$key]['detail'][$key_detail]['note']         = $value->note;
                    }
                }
            }

            return response()->json([
                'message' => "success",
                'data' => $data
            ], 200);
        }

        return response()->json([
            'message' => "Gagal mengambil data order"
        ], 404);
    }

    public function detail($id)
    {
        $order        = $this->objOrder->getDataDetail($id);

        if($order)
        {
            switch ($order->status_order) {
                case '0':
                    $status = "Pending";
                    break;

                case '1':
                    $status = "Diterima";
                    break;

                case '2':
                    $status = "Persiapan Pengiriman";
                    break;

                case '3':
                    $status = "Dalam Pengiriman";
                    break;

                case '4':
                    $status = "Batal";
                    break;

                case '5':
                    $status = "Selesai";
                    break;

                case '6':
                    $status = "Pindahan";
                    break;

                default:
                    $status = "";
                    break;
            }

            $image = "";

            if(isset($order->Mitra) && $order->Mitra->avatar != "") {
                $image = $this->cdnMitra.$order->Mitra->avatar;
            }

            $data['orders']['orders_id']              = $order->order_id;
            $data['orders']['order_code']             = $order->order_code;
            $data['orders']['fulname']                = isset($order->Mitra) ? $order->Mitra->name : "";
            $data['orders']['name_store']             = isset($order->Mitra) ? $order->Mitra->name_store : "";
            $data['orders']['store_phone']            = isset($order->Mitra) ? $order->Mitra->mobile : "";
            $data['orders']['address']                = $order->address;
            $data['orders']['mitra_address']          = isset($order->Mitra) ? $order->Mitra->address : "";
            $data['orders']['mitra_latitude']         = isset($order->Mitra) ? $order->Mitra->latitude : "";
            $data['orders']['mitra_longitude']        = isset($order->Mitra) ? $order->Mitra->longitude : "";
            $data['orders']['address']                = $order->address;
            $data['orders']['latitude']               = $order->latitude;
            $data['orders']['longitude']              = $order->longitude;
            $data['orders']['address_note']           = $order->address_note;
            $data['orders']['courier_id']             = $order->courier_id;
            $data['orders']['courier_name']           = isset($order->Courier) ? $order->Courier->name : "";
            $data['orders']['customer_name']          = isset($order->Customer) ? $order->Customer->name : "";
            $data['orders']['customer_phone']         = isset($order->Customer) ? $order->Customer->mobile : "";
            $data['orders']['customer_mobile']         = isset($order->Customer) ? $order->Customer->mobile : "";
            $data['orders']['customer_address']       = $order->address;
            $data['orders']['customer_latitude']      = $order->latitude;
            $data['orders']['customer_longitude']     = $order->longitude;
            $data['orders']['customer_avatar']        = "";
            $data['orders']['payment']                = $order->payment;
            $data['orders']['status_payment']         = $order->status_payment;
            $data['orders']['reviews']                = $order->review;
            $data['orders']['stars']                  = $order->star;
            $data['orders']['status']                 = $status;
            $data['orders']['discount']               = $order->discount;
            $data['orders']['ongkir']                 = $order->ongkir;
            $data['orders']['total_price']            = $order->total_price;
            $data['orders']['delivery_date']          = $order->delivery_date;
            $data['orders']['order_date']             = date_format(date_create($order->order_date),'Y-m-d H:i:s');
            $data['orders']['finish_date']            = date_format(date_create($order->finish_date),'Y-m-d H:i:s');
            $data['orders']['image']                  = $image;

            //detail
            $orderDetail                    = OrderDetail::where('order_id', $id)->get();

            foreach ($orderDetail as $key => $value) {
                $data['detail'][$key]['id']           = $value->id;
                $data['detail'][$key]['name']         = isset($value->Product) ? $value->Product->name : "";;
                $data['detail'][$key]['qty']          = $value->qty;
                $data['detail'][$key]['price']        = $value->price;
                $data['detail'][$key]['subtotal']     = ($value->qty * $value->price);
                $data['detail'][$key]['note']         = $value->note;
            }

            return response()->json([
                'message' => "success",
                'data' => $data
            ], 200);
        }

        return response()->json([
            'message' => "Gagal mengambil data order"
        ], 404);
    }

    public function isCancel(Request $request, $id)
    {
        $orders = Order::find($id);

        if ($orders) {

            $orders->status       = 4;
            $orders->note         = $request->note;
            $orders->updated_at   = date('Y-m-d H:i:s');
            $orders->update();

            //sendnotif delivery
            $customer     = Customer::find($orders->user_id);
            $mitra        = Mitra::find($orders->mitra_id);

            $token_fcm    = $mitra->token_fcm;
            $title        = "Order Telah dibatalkan oleh : ". $customer->name;
            $description  = "Order #". $orders->order_code ." telah dibatalkan";
            $type         = "Order";

            $this->notif->sendCancel($token_fcm, $title, $description, $type, $orders->user_id, $orders->id, $orders->mitra_id);

            return response()->json( array( 'message' => 'Berhasil membatalkan order'), 200 );
        }

        $data = ['message' => 'Gagal melakukan pembatalan order'];

        return response()->json( $data , 500 );

    }

    /**
     * [checkout description]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function checkout(Request $request)
    {
        $params = $request->all();
        $order  = new Order;

        \DB::beginTransaction();
        try {

            $user       = Customer::find($this->user->id);

            //update data user
            // $user->address    = $params['address'];
            // $user->latitude   = $params['latitude'];
            // $user->longitude  = $params['longitude'];
            // $user->save();

            $latitude   = $params['latitude'] == "" ? 0 : $params['latitude'];
            $longitude  = $params['longitude'] == "" ? 0 : $params['longitude'];
            $payment    = Payment::find($params['payment_id']);

            $distance   = Radius::where('is_radius_reject', 0)->first();
            $distance_radius = $distance->radius;

            //
            $except      = array();

            if(app('session')->has('mitra_id')) { $except = app('session')->get('mitra_id'); }

            $mitra_id   = $order->getByDistance($latitude, $longitude, $distance_radius, $except);

            if(empty($mitra_id))
            {
                \DB::rollback();
                $result = [
                    'message' => 'Tidak ada mitra yang menjangkau pesanan Anda, silahkan gunakan alamat lain.',
                ];

                return response()->json( $result, 404 );
            }
            else {

                //check limit
                $order_limit = 0;

                if(isset($params['voucher_id']))
                {
                    if($params['voucher_id'] != NULL || $params['voucher_id'] != "") {
                        $voucher    = Voucher::find($params['voucher_id']);
                        $order_limit = Order::where('user_id', $user->id)->where('voucher_id', $voucher->id)->count();
                    }
                }

                if($order_limit == 0) {
                    $order                    = new Order;

                    if(isset($params['voucher_id'])) {
                        $voucher    = Voucher::find($params['voucher_id']);
                        if ($voucher) {
                            $order->discount          = $voucher->discount == "" ? 0 : $voucher->discount;
                            $order->voucher_id        = $voucher->id;
                        }
                    }

                    $order->order_code        = Order::getAutoNumber('ORD', 0);
                    $order->user_id           = $this->user->id;
                    $order->mitra_id          = $mitra_id;
                    $order->payment           = $payment->payment;
                    $order->status_payment    = 'pending';
                    $order->is_confirmation   = isset($params['bank_account']) ? 0 : 1;
                    $order->is_store          = 1;
                    $order->status            = 0;
                    $order->ongkir            = 0;
                    $order->address           = $params['address'];
                    $order->address_note      = $params['address_note'];
                    $order->latitude          = $params['latitude'];
                    $order->longitude         = $params['longitude'];

                    //if payment is transfer
                    if(isset($params['bank_account']))
                    {
                        if($params['bank_account'] != null || isset($params['bank_account']))
                        {
                            $order->bank_account_name   = $params['bank_account']['name'];
                            $order->bank_account_number = $params['bank_account']['norek'];
                        }
                    }

                    $order->save();

                    if (count($request->product_id) > 0) {
                        for ($i=0; $i < sizeof($request->product_id); $i++) {
                            if ($request->product_id[$i] != '') {
                                $product                = Product::find($request->product_id[$i]);

                                if($request->city != "" && $request->city != "Bandung" && $request->city != "Kota Bandung" && $request->city != "Kab Bandung" && $request->city != "bandung") {
                                    $product            = ProductRegion::select(['product_id as id','price as price_market'])
                                                            ->where('product_id', $request->product_id[$i])->first();
                                }

                                $detail                 = new OrderDetail();
                                $detail->order_id       = $order->id;
                                $detail->product_id     = $product->id;
                                $detail->qty            = $params['qty'][$i];
                                $detail->price          = $product->price_market;
                                $detail->note           = $params['note'][$i];

                                $detail->save();
                            }
                        }
                    }

                    $details        = array();
                    $order_details  = OrderDetail::where('order_id', $order->id)->get();

                    foreach ($order_details as $key => $detail) {
                        $details[$key]['product_id']    = $detail->product_id;
                        $details[$key]['product_name']  = $detail->product_name;
                        $details[$key]['qty']           = $detail->qty;
                        $details[$key]['price']         = $detail->price;
                        $details[$key]['note']          = $detail->note;
                    }

                    $d_voucher = null;

                    if(isset($params['voucher_id'])) {
                        $voucher    = Voucher::find($params['voucher_id']);
                        if($voucher) {
                            $d_voucher  = array(
                                'id' => $voucher->id,
                                'code' => $voucher->code,
                                'discount' => $voucher->value,
                            );
                        }
                    }

                    //get data bank_account
                    $bank_account = $this->modulStores->getDataBank($mitra_id);

                    $data = array(
                        'order_code'      => $order->order_code,
                        'user_id'         => $order->user_id,
                        'role'            => $order->role,
                        'payment'         => $order->payment,
                        'status_payment'  => $order->status_payment,
                        'status'          => $order->status == 0 ? 'pending' : '',
                        'address'         => $order->address,
                        'address_note'    => $order->address_note,
                        'ongkir'          => $order->ongkir,
                        'discount'        => $order->discount,
                        'voucher'         => $d_voucher,
                        'order_details'   => $details,
                        'bank_account'    => $bank_account
                    );


                    $result = [
                        'message' => 'success',
                        'data'    => $data
                    ];

                    //sendNotif
                    $mitra        = Store::find($mitra_id);
                    $token_fcm    = $mitra->token_fcm;
                    $title        = "Order Baru";
                    $description  = "Toko anda mendapatkan pesanan baru";
                    $type         = "Order";;

                    $this->notif->sendNotif($token_fcm, $title, $description, $type, $mitra_id, $order->id);
                    // exit;
                    \DB::commit();

                    return response()->json( $result, 200 );
                }

                else {
                    \DB::rollback();
                    $result = [
                        'message' => 'Anda sudah melebihi batas penggunaan voucher',
                    ];

                    return response()->json( $result, 404 );
                }
            }
        } catch(\Exception $e) {
            \DB::rollback();

            $result = [
                'message' => $e->getMessage().' '.$e->getLine(),
            ];

            return response()->json( array( (object) $result ), 500 );
        }
    }

    public function reviews(Request $request, $id)
    {
        $orders = Order::find($id);

        if($orders)
        {
            if($orders->reviews == NULL)
            {
                $orders->review  = $request->reviews;
                $orders->star    = $request->stars;
                $orders->save();

                $code = 200;
                $data = ['message' => 'Berhasil menuliskan ulasan order'];
            } else {
                $code = 403;
                $data = ['message' => 'Ulasan sudah pernah ditulis'];
            }

            return response()->json( $data, $code );
        }


        return response()->json( ['message' => 'Gagal menambahkan ulasan'], 403 );
    }
}
