<?php

namespace App\Http\Controllers\Customer;

//Models
use App\Models\Ticket;
use App\Models\Customer;
use App\Models\Mitra;
use App\Models\Store;

use Firebase\JWT\JWT;

use Illuminate\Http\Request;
use Laravel\Lumen\Routing\Controller as BaseController;

class TicketController extends BaseController
{
	
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->token       = $request->token;
        $this->credentials = JWT::decode($this->token, env('JWT_SECRET'), ['HS256']);

        $this->user        = Customer::find($this->credentials->sub);
        $this->cdnTicket   = "../../cdn/tickets/";
    }
	
    public function index()
    {
		$ticket      = Ticket::where('user_id', $this->user->id)->get();
		$data 		 = array();

        if($ticket && count($ticket) > 0)
        {
			//get data payment
			foreach ($ticket as $key => $value) {
				$data[$key]['id'] 		    = $value->id;
				$data[$key]['complaint'] 	= $value->complaint;
				$data[$key]['name'] 	    = $value->Customer->name;
				$data[$key]['agent'] 	    = $value->Agent->name;
				$data[$key]['store'] 	    = $value->Store->name;
				$data[$key]['datetime'] 	= date('d M Y H:i:s', strtotime($value->created_at));
			}

            return response()->json([
                'message' => "success",
                'data' => $data
            ], 200);
        }

        return response()->json([
            'message' => "Gagal mengambil data keluhan"
        ], 500);
    }

    public function store(Request $request)
    {
        try 
        {
            $ticket                 = new Ticket;
            $ticket->complaint      = $request->complaint;
            $ticket->title          = $request->title;
            $ticket->name           = $request->name;
            $ticket->email          = $request->email;
            $ticket->customer_id    = $this->user->id;

            if($request->hasFile('image')){
                $image = $this->storeFile($_FILES['image'], 0, '');
                $ticket->image = $image;
            }

            $ticket->save();

            return response()->json([ 
                'message' => "Terima Kasih sudah memberikan kritik / saran kepada kami.", 'data' => $ticket 
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage().' line : '.$e->getLine()
            ], 500);
        }
    }

    function storeFile($ticket, $old = '', $user = '')
    {
        $userfile       = explode(".", strtolower($ticket['image']));

        $filename       = $userfile[0];
        $fileext        = $userfile[1];

        $file           = md5($filename).".".$fileext;

        $target_dir     = $this->cdnTicket;
        $target_file    = $target_dir . basename($file);
        $uploadOk       = 1;
        $imageFileType  = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Check if image file is a actual image or fake image
        if(isset($_POST["submit"])) {
            $check = getimagesize($_FILES["image"]["tmp_name"]);
            if($check !== false) {
                echo "File is an image - " . $check["mime"] . ".";
                $uploadOk = 1;
            } else {
                echo "File is not an image.";
                $uploadOk = 0;
            }
        }

        // Check file size
        if ($_FILES["image"]["size"] > 500000) {
            echo "Sorry, your file is too large.";
            $uploadOk = 0;
        }
        // Allow certain file formats
        if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
        && $imageFileType != "gif" ) {
            echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
            $uploadOk = 0;
        }
        // Check if $uploadOk is set to 0 by an error
        if ($uploadOk == 0) {
            echo "Sorry, your file was not uploaded.";
        // if everything is ok, try to upload file
        } else {
            if (move_uploaded_file($_FILES["image"]["tmp_name"], $target_file)) {

                return $file;

            } else {
                echo "Sorry, there was an error uploading your file.";
            }
        }
    }
}
