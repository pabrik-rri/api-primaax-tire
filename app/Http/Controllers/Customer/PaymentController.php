<?php

namespace App\Http\Controllers\Customer;

use App\Models\Payment;
use Laravel\Lumen\Routing\Controller as BaseController;

class PaymentController extends BaseController
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->cdn         = config('app.cdn');
        $this->cdnPayment  = $this->cdn."/payment_methods/";
	}
	
    public function index()
    {
		$payment      = Payment::where('status',1)->get();
		$data 		  = array();

        if($payment && count($payment) > 0)
        {
			//get data payment
			foreach ($payment as $key => $value) {
				//check payment
				$images = $value->image == "" ? $this->cdn.'/no_image_available.png' : $this->cdnPayment.$value->image;

				$data[$key]['id'] 		= $value->id;
				$data[$key]['payment'] 	= $value->payment;
				$data[$key]['images'] 	= $images;
			}

            return response()->json([
                'message' => "success",
                'data' => $data
            ], 200);
        }

        return response()->json([
            'message' => "Gagal mengambil data metode pembayaran"
        ], 500);
    }
}
