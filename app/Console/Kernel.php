<?php

namespace App\Console;

use App\Models\ProductMitra;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            $fcmUrl   = "https://fcm.googleapis.com/fcm/send";
            $apiKey   = "AAAAHY2GRpQ:APA91bHpMF_XaIt5Lpf5dLi4n6pxh_eq7I7IfCRwaMdDS8gtumKjegc4fbBEf4xUmFbhKXA2Hzy9R-WcT8lqZIEyjvy1f9bsn495_49YG4EracCHvzDHGUVCdIT9JLbV0OPRvH-Y_kHP2I_fRdQD9JWi1HpWsoHSkA";

            $mitra_alert = ProductMitra::select([
                                'product_mitras.mitra_id as mitra_id',
                                'mitras.token_fcm as token_fcm',
                                'products.name as name',
                                'product_mitras.min_qty as min_qty',
                            ])
                            ->join('products','product_mitras.product_id', '=' ,'products.id')
                            ->join('mitras','product_mitras.mitra_id', '=' ,'mitras.id')
                            ->where('product_mitras.qty','<=', \DB::raw('product_mitras.min_qty'))
                            ->get();

            if(sizeof($mitra_alert) > 0)
            {
                foreach ($mitra_alert as $key => $value)
                {
                    $extraNotificationData  = [
                        "title"         => "Stok Minimum",
                        "message"       => "Product ". $value->name ." sudah mencapai batas minimum, silahkan segera melakukan PO",
                        "type"          => "stock"
                    ];

                    $fcmNotification = [
                        // 'registration_ids' => $tokenList, //multple token array
                        'to'             => $value->token_fcm, //single token
                        // 'notification'       => $notification,
                        'priority'          => 'high',
                        'delay_while_idle'  => false,
                        'time_to_live'      => 0,
                        'data'              => $extraNotificationData
                    ];

                    $fields = json_encode($fcmNotification);

                    $headers = [
                        'Authorization: key='. $apiKey,
                        'Content-Type: application/json'
                    ];

                    $ch = curl_init();
                    curl_setopt($ch, CURLOPT_URL, $fcmUrl);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $fields);
                    $result = curl_exec($ch);

                    curl_close($ch);
                }
            }

        })->hourly();
    }
}
