<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'v2'], function () use ($router) {

	//mitra
	$router->group(['prefix' => 'mitra', 'namespace' => 'Mitra'], function () use ($router) {
		//login
		$router->post( 'login', ['uses' => 'AuthController@authenticate']);

		//jwt.auth
		$router->group(['middleware' => 'jwt.auth'], function() use ($router) {
			//profile
	        $router->get('profile', ['uses' => 'AuthController@profile']);
            $router->post('profile/update', ['uses' => 'AuthController@updateProfile']);

	        $router->post('store/status',       ['uses' => 'AuthController@updateStore']);
            $router->get('store/list',          ['uses' => 'ScheduleController@index']);
            $router->post('store/setting',      ['uses' => 'ScheduleController@create']);
            $router->post('store/update/{id}',  ['uses' => 'ScheduleController@create']);

			//homepage
	        $router->get('homepage', ['uses' => 'HomepageController@index']);

			//faq
	        $router->get('faq', ['uses' => 'FaqController@index']);
	        $router->get('faq/detail/{id}', ['uses' => 'FaqController@detail']);

			//courier
	        $router->get('courier', ['uses' => 'CourierController@index']);
	        $router->post('courier/create', ['uses' => 'CourierController@store']);
	        $router->post('courier/update/{id}', ['uses' => 'CourierController@update']);
            $router->get('courier/delete/{id}', ['uses' => 'CourierController@destroy']);
	        $router->post('courier/resetpassword', ['uses' => 'CourierController@resetPassword']);

	        //order
	        $router->get('orders/list', ['uses' => 'OrderController@index']);
	        $router->get('orders/detail/{id}', ['uses' => 'OrderController@detail']);
	        $router->get('orders/history', ['uses' => 'OrderController@history']);
	        $router->post('orders/update', ['uses' => 'OrderController@update']);
	        $router->post('orders/accept/{id}', ['uses' => 'OrderController@isAccept']);
	        $router->post('orders/adjustment/{id}', ['uses' => 'OrderController@isAdjustment']);
	        $router->post('orders/delivery/{id}', ['uses' => 'OrderController@isDelivery']);
	        $router->post('orders', ['uses' => 'OrderController@checkout']);

	        $router->get('orders/income', ['uses' => 'OrderController@income']);

            //po
            $router->get('product/po', ['uses' => 'ProductController@index']);
            $router->post('product/po/update', ['uses' => 'ProductController@update']);
			$router->post('po/adjustment/{id}', ['uses' => 'OrderController@isAdjustmentPO']);
			
            //debt
            $router->get('debt', ['uses' => 'DebetController@index']);
            $router->get('debt/add', ['uses' => 'DebetController@create']);
            $router->post('debt/payment', ['uses' => 'DebetController@store']);

	        //fcm update
	        $router->post('fcm', ['uses' => 'AuthController@updateFcm']);
	        $router->post('refresh/token', ['uses' => 'AuthController@refreshToken']);
		});
	});

	//customer
	$router->group(['prefix' => 'customer', 'namespace' => 'Customer'], function () use ($router) {
		//login
		$router->post('signin', ['uses' => 'AuthController@authenticate']);
		$router->post('register', ['uses' => 'AuthController@store']);

		//jwt.auth
		$router->group(['middleware' => 'jwt.auth'], function() use ($router) {
			//profile
	        $router->get('profile', ['uses' => 'AuthController@profile']);
	        $router->post('profile/update', ['uses' => 'AuthController@updateProfile']);

			//homepage
	        $router->get('homepage', ['uses' => 'HomepageController@index']);

			//faq
	        $router->get('faq', ['uses' => 'FaqController@index']);
			$router->get('faq/detail/{id}', ['uses' => 'FaqController@detail']);

			//address
	        $router->get('address', ['uses' => 'AddressController@index']);
			$router->post('address/create', ['uses' => 'AddressController@store']);
			$router->post('address/delete/{id}', ['uses' => 'AddressController@destroy']);

			//notification
	        $router->get('notification', ['uses' => 'NotificationController@index']);

	        //order
	        $router->get('orders/list', ['uses' => 'OrderController@index']);
	        $router->get('orders/detail/{id}', ['uses' => 'OrderController@detail']);
	        $router->get('orders/history', ['uses' => 'OrderController@history']);
	        $router->post('orders/cancel/{id}', ['uses' => 'OrderController@isCancel']);
	        $router->post('orders/reviews/{id}', ['uses' => 'OrderController@reviews']);
			$router->post('orders', ['uses' => 'OrderController@checkout']);
			
	        //fcm update
			$router->post('fcm', ['uses' => 'AuthController@updateFcm']);
	        $router->post('refresh/token', ['uses' => 'AuthController@refreshToken']);
			
			//ticket
	        $router->get('ticket', ['uses' => 'TicketController@index']);
	        $router->post('ticket/add', ['uses' => 'TicketController@store']);
		});
	});

	//CMS
	$router->group(['prefix' => 'cms', 'namespace' => 'Cms'], function () use ($router) {
		//Slider
		$router->get('slider', ['uses' => 'SliderController@index']);

		//Gallery
		$router->get('gallery', ['uses' => 'GalleryController@index']);

		//News
		$router->get('news', ['uses' => 'NewsController@index']);
		$router->get('news/detail/{id}', ['uses' => 'NewsController@show']);

		//Contact
		$router->post('contact', ['uses' => 'ContactController@index']);

		//Sosmed
		$router->get('sosmed', ['uses' => 'SosmedController@index']);

		//About
		$router->get('about', ['uses' => 'AboutController@index']);
		$router->get('about/detail/{id}', ['uses' => 'AboutController@show']);

		//City
		$router->get('cities', ['uses' => 'CityController@index']);

		//Store
		$router->get('stores', ['uses' => 'StoreController@index']);

		//Partner
		$router->get('partners', ['uses' => 'PartnerController@index']);

		//Home
		$router->get('home/product', ['uses' => 'HomeController@product']);
		$router->get('home/news', ['uses' => 'HomeController@news']);
		$router->get('home/about', ['uses' => 'HomeController@about']);
	});

	//jwt.auth
	$router->group(['middleware' => 'jwt.auth'], function() use ($router) {
		//notification
		$router->get('notification', ['uses' => 'NotificationController@index']);
		$router->get('customer/notification', ['uses' => 'NotificationController@customer']);
	});

	//area
	$router->get('province', ['uses' => 'AreaController@province']);
	$router->get('city', ['uses' => 'AreaController@city']);

	//without jwt
	$router->get('customer/faq', [ 'uses' => 'Customer\FaqController@index']);
	$router->get('customer/home', ['uses' => 'Customer\HomepageController@index']);
	$router->get('customer/product', ['uses' => 'ProductController@index']);
	$router->get('customer/product/category', ['uses' => 'ProductController@productCategory']);
	$router->get('customer/banners', ['uses' => 'BannerController@index']);
	$router->get('customer/voucher', ['uses' => 'Customer\VoucherController@index']);
	$router->post('customer/orders/reedem', ['uses' => 'Customer\VoucherController@reedem']);

	//payment channel list
	$router->get('customer/payment', ['uses' => 'Customer\PaymentController@index']);
			
	$router->get('product', ['uses' => 'ProductController@index']);
    $router->get('product/category', ['uses' => 'ProductController@productCategory']);
    $router->get('product/po', ['uses' => 'ProductController@productPO']);
	$router->get('category', ['uses' => 'ProductController@categories']);

	$router->post('customer/forgotpassword', ['uses' => 'Customer\AuthController@forgotPassword']);
	$router->post('customer/newpassword', ['uses' => 'Customer\AuthController@updatePassword']);

	$router->post('mitra/forgotpassword', ['uses' => 'Mitra\AuthController@forgotPassword']);
	$router->post('mitra/newpassword', ['uses' => 'Mitra\AuthController@updatePassword']);

	//get store
	$router->get('customer/store/list', function (Request $request) use ($router) {
		try 
        {
			$store = \App\Models\Store::where('is_courier',0)->orderBy('name')->get();
			
			if($request->agent_id != null)
			{
				$agent_id = $request->agent_id;

				$store = \App\Models\Store::where('is_courier',0)
							->where('is_courier',0)
							->where('mitra_id',$agent_id)
							->orderBy('name')->get();
			}

            return response()->json([ 
                'message' => "success", 'data' => $store 
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage().' line : '.$e->getLine()
            ], 500);
        }
	});

	//get agent
	$router->get('customer/agent/list', function () use ($router) {
		try 
        {
            $agent = \App\Models\Mitra::where('is_courier',0)->orderBy('name')->get();

            return response()->json([ 
                'message' => "success", 'data' => $agent 
            ], 200);

        } catch (\Exception $e) {
            return response()->json([
                'message' => $e->getMessage().' line : '.$e->getLine()
            ], 500);
        }
	});

});
