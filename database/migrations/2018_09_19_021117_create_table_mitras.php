<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableMitras extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('mitras');

        Schema::create('mitras', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 50)->nullable();
            $table->string('no_id_card', 50)->nullable();
            $table->string('mobile', 50)->nullable();
            $table->string('email', 255)->nullable();
            $table->text('name_store');
            $table->text('password');
            $table->char('gender')->nullable()->comment('1 : Male, 2 : Female');
            $table->date('birth_date')->nullable();
            $table->text('address')->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->integer('city_id')->unsigned();
            $table->char('zip_code', 6)->nullable();
            $table->integer('status')->default(0)->comment('0 : Unactive , 1 : Active');
            $table->text('token_fcm')->nullable();
            $table->text('avatar')->nullable();
            $table->integer('is_courier')->default(0)->comment('0 : Not Courier , 1 : Courier');
            $table->integer('mitra_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mitras');
    }
}
