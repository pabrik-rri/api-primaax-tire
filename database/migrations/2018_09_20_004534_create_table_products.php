<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('products');

        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('code');
            $table->string('name')->nullable();
            $table->text('description')->nullable();
            $table->float('price_market')->default(0);
            $table->float('price_warehouse')->default(0);
            $table->integer('qty')->default(0);
            $table->integer('min_qty')->nullable();
            $table->text('image')->nullable();
            $table->integer('product_category_id')->unsigned();
            $table->integer('unit_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
