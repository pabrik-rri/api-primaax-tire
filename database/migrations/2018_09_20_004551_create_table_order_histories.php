<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderHistories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('order_histories');
        
        Schema::create('order_histories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qty_old')->nullable();
            $table->integer('qty')->nullable();
            $table->integer('order_id')->unsigned();
            $table->integer('order_detail_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_histories');
    }
}
