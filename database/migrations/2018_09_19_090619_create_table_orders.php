<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('orders');

        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_code', 50)->nullable();
            $table->string('payment', 255)->nullable();
            $table->string('status_payment', 255)->nullable();
            $table->string('status', 50)->nullable()->comment('Order Status');
            $table->text('address')->nullable();
            $table->string('address_note', 50)->nullable();
            $table->double('latitude')->nullable();
            $table->double('longitude')->nullable();
            $table->integer('ongkir')->nullable();
            $table->integer('discount')->nullable();
            $table->text('note')->nullable();
            $table->text('review')->nullable();
            $table->integer('star')->nullable();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('mitra_id')->unsigned()->nullable();
            $table->integer('courier_id')->unsigned()->nullable();
            $table->integer('voucher_id')->unsigned()->nullable();
            $table->datetime('delivery_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
