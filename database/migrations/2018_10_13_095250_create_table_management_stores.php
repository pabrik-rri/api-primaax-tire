<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableManagementStores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('management_stores');

        Schema::create('management_stores', function (Blueprint $table) {
            $table->increments('id');
            $table->string('day', 40)->nullable();
            $table->string('open_time', 10)->nullable();
            $table->string('closing_time', 10)->nullable();
            $table->integer('status')->nullable()->comment("1:open, 0:close")->default(1);
            $table->integer('mitra_id')->unsigned()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('management_stores');
    }
}
