<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('order_details');
        
        Schema::create('order_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('qty')->default(0);
            $table->float('price')->default(0);
            $table->text('note')->nullable();
            $table->integer('order_id')->unsigned();
            $table->integer('product_id')->unsigned();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
